import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:omnilight/config.dart';
import 'package:omnilight/main.dart';
import 'package:omnilight/model.dart';
import 'package:omnilight/network.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'global.dart';

class GameDesc {
  String? name;
  bool? isTest;
  String? url;
  String? description;

  GameDesc.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    isTest = json['isTest'];
    if (isTest == null) isTest = false;
    url = json['url'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> ret = Map();
    ret['name'] = name;
    ret['isTest'] = isTest;
    ret['url'] = url;
    ret['description'] = description;
    return ret;
  }
}

class LoadStatus {
  late String status;
  bool isSelect = false;
  bool isError = false;

  LoadStatus() {
    status = "";
  }

  LoadStatus.status(String st) {
    status = st;
  }

  LoadStatus.select() {
    status = "Select";
    isSelect = true;
  }

  LoadStatus.error(String st) {
    isError = true;
    status = st;
  }
}

class LoadManager {
  late StreamController<LoadStatus> status;

  late GameDesc currentGame;
  String? loadUri = "";
  int gameListSerial = -1;
  List<GameDesc> games = [];

  void dispose() {
    status.close();
  }

  void startLoading() {
    status = StreamController();
    runApp(LoadApp());
    status.add(LoadStatus.status("Загрузка настроек"));
    SharedPreferences.getInstance().then((i) {
      preferences = i;
      loadPrefStatus();
      //lastState.loadPrefsData();
      //lastState.onTimer();
    });
  }

  void loadPrefStatus() {
    loadUri = preferences!.getString("GameURI");
    if (loadUri == null) loadUri = "https://games.mg-lpoint.ru/games.json";

    String? g = preferences!.getString("CurrentGame");
    if (g == null)
      startSelectGame();
    else {
      try {
        currentGame = GameDesc.fromJson(jsonDecode(g));
        loadGame();
      } catch (e) {
        print("Error loading currentGame $e");
        startSelectGame();
        return;
      }
    }
    status.add(LoadStatus.status("Настройки загружены"));
  }

  void handleGamesDescription(Response res) {
    if (res.statusCode == 200) {
      if (res.data is Map<String, dynamic>) {
        int newserial = res.data['serial'];
        if (newserial <= gameListSerial) {
          print("No updates. Serial $newserial $gameListSerial");
          status.add(LoadStatus.select());
          return;
        }
        gameListSerial = newserial;
        print("new serial is $newserial");
        games.clear();
        for (Map<String, dynamic> game in res.data['games']) {
          games.add(GameDesc.fromJson(game));
        }
        status.add(LoadStatus.select());
      }
    } else {
      status.add(
          LoadStatus.error("Ошибка ${res.statusCode} ${res.statusMessage}"));
    }
  }

  void handleGameDescription(Response res) {
    if (res.statusCode == 200) {
      if (res.data is Map<String, dynamic>) {
        try {
          Configuration config = Configuration.fromJson(res.data);
          //TODO: здесь дожно быть кешировние конига
          configuration = config;
          net = NetworkEngine();
          lastState = StateStore();
          stateStreamController = StreamController.broadcast();
          lastState.loadPrefsData();
          lastState.onTimer();

          runApp(MyApp());
        } on TypeError catch (e) {
          String msg = "handleGameDescription error $e ${e.stackTrace}";
          print(msg);
          status.add(LoadStatus.error(msg));
        } catch (e) {
          String msg = "handleGameDescription error $e ";
          print(msg);
          status.add(LoadStatus.error(msg));
        }
      }
    } else {
      status.add(
          LoadStatus.error("Ошибка ${res.statusCode} ${res.statusMessage}"));
    }
  }

  void handleError(Object obj) {
    print(obj);
    if (obj is DioError) {
      DioError err = obj;
      status.add(LoadStatus.error(err.message));
      return;
    }
    status.add(LoadStatus.error("Ошибка загрузки данных"));
  }

  void loadGamesDescription() {
    var client = Dio();
    client
        .get(loadUri!)
        .then(this.handleGamesDescription, onError: this.handleError);
  }

  void startSelectGame() {
    status.add(LoadStatus.select());
    loadGamesDescription();
  }

  void selectGame(GameDesc game) {
    currentGame = game;
    preferences!.setString("GameURI", loadUri!);
    preferences!.setString("CurrentGame", jsonEncode(currentGame.toJson()));
    runApp(LoadApp());
    loadGame();
  }

  void loadGame() {
    var gameuri = currentGame.url! + "/game.json";
    status.add(LoadStatus.status("Загрука описания игры c адреса $gameuri"));
    var client = Dio();
    client
        .get(gameuri)
        .then(this.handleGameDescription, onError: this.handleError);
  }
}

class LoadApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: LoadAppHome(),
        title: "OmnilightLoader",
        routes: <String, WidgetBuilder>{
          "/details": (BuildContext ctx) => GameDetails(),
        });
  }
}

class LoadAppHome extends StatelessWidget {
  Widget showProgress(String message) {
    return Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [CircularProgressIndicator(), Text(message)])));
  }

  List<Widget> getGamesList(BuildContext context) {
    List<Widget> ret = [];
    for (var g in loadManager.games) {
      ret.add(ListTile(
        title: Text(g.name!),
        subtitle: Text(g.description!),
        onTap: () => Navigator.of(context).pushNamed("/details", arguments: g),
      ));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: null,
      stream: loadManager.status.stream,
      builder: (BuildContext ctx, AsyncSnapshot<LoadStatus?> status) {
        if (status.connectionState != ConnectionState.active ||
            status.data == null) return showProgress("Загрузка...");
        if (status.data!.isSelect) {
          return Scaffold(
              appBar: AppBar(title: Text("Список игр")),
              body: ListView(children: getGamesList(context)));
        }
        if (status.data!.isError) {
          return AlertDialog(
            title: Text("Ошибка"),
            content: Text(status.data!.status),
            actions: [
              TextButton(
                  onPressed: () => loadManager.startSelectGame(),
                  child: Text("Закрыть"))
            ],
          );
        }
        return showProgress(status.data!.status);
      },
    );
  }
}

//Details
class GameDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GameDesc game = ModalRoute.of(context)!.settings.arguments as GameDesc;
    return Scaffold(
      appBar: AppBar(
        title: Text(game.name!),
      ),
      body: Text(game.description!),
      bottomNavigationBar: BottomAppBar(
          child: RaisedButton(
        child: Text("Выбрать"),
        onPressed: () {
          loadManager.selectGame(game);
          Navigator.of(context).pop();
        },
      )),
    );
  }
}
