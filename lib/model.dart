import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:omnilight/blescanner.dart';
import 'package:omnilight/global.dart';

import 'events.dart';
import 'global.dart';
import 'inventory.dart';
import 'network.dart';

typedef void FeatureUpdatedFunc(FeatureDesc feat);

class FeatureTypeDesc {
  late int pk;
  late String name;
  late bool tradable;
  late bool replicable;
  late bool expectsValue;
  late bool integer;
  late bool nonNegative;
  late bool unique;
  late bool uniqueWithValue;
  late bool editable;
  late bool static;
  Map<String, dynamic> values = Map();
  StreamController<Map<String, dynamic>>? scGlobalUpdates;
  String group = "";
  List<TransitionComponentDesc> relatedTransitions = [];

  FeatureTypeDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    tradable = loadAndCheck(json, "tradable");
    replicable = loadAndCheck(json, "replicable");
    expectsValue = loadAndCheck(json, "expects_value");
    integer = loadAndCheck(json, "integer");
    nonNegative = loadAndCheck(json, "non_negative");
    unique = loadAndCheck(json, "non_negative");
    uniqueWithValue = loadAndCheck(json, "non_negative");
    editable = loadAndCheck(json, "non_negative");
    static = loadAndCheck(json, "static");
    if (tradable) {
      for (var kv in configuration.inventoryGroups.entries) {
        if (kv.value.contains(name)) group = kv.key;
      }
      if (group.isEmpty) {
        if (integer)
          group = "Ресурсы";
        else
          group = "Предметы";
      }
    }
    if (static) reloadValues();
  }

  Stream<Map<String, dynamic>> getUpdates() {
    if (scGlobalUpdates == null) {
      scGlobalUpdates = StreamController.broadcast(onListen: () {
        print("$name updates lisened");
        lastState.globalListeners.add(this);
      }, onCancel: () {
        print("$name updates canceled");
        lastState.globalListeners.remove(this);
      });
    }
    return scGlobalUpdates!.stream;
  }

  Future<void> reloadValues() async {
    Request rq = await net.addToQueue(
        "GET", ["character", "feature-types", pk.toString()], "");
    Map<String, dynamic>? json = rq.response!.data;
    if (json == null) {
      lastState.addError(Error.network(
          0, "Wrong data on feature type update ${rq.response!.data}"));
      return;
    }
    values = json["values"] ?? Map();
    print("Loaded global values $values");
    scGlobalUpdates?.add(values);
  }

  bool updateValue(String id, dynamic val) {
    if (values.containsKey(id) && values[id] == val) return false;
    values[id] = val;
    scGlobalUpdates?.add(values);
    return true;
  }

  void invalidateRelatedTransitions() {
    print("$name updated: all related transition invalidated");
    for (var t in relatedTransitions) t.invalidate();
    relatedTransitions.clear();
  }
}

class PlayerDesc {
  late int id;
  late String nickname;
  late String realName;
  late String location;
  late List<String> contacts = [];
  late bool paid;
  late String medicalInfo;
  late int character;

  //TODO: обрабока кейса, когда нет активного персонажа
  PlayerDesc.fromJson(Map<String, dynamic> json) {
    id = loadAndCheck(json, "pk");
    nickname = loadAndCheck(json, "nickname");
    realName = loadOrDefault(json, "real_name", "");
    location = json["location"] ?? "NOLOCATION";
    paid = loadOrDefault(json, "paid", false);
    medicalInfo = loadOrDefault(json, "medical_info", "");
    character = loadAndCheck(json, "character");
  }

  static Future<PlayerDesc> load(int id) async {
    var resp = await net.addToQueue("get", ["player", id.toString()], "");
    var plr = PlayerDesc.fromJson(resp.response!.data);
    return plr;
  }
}

class FeatureDesc {
  late int type;
  late String name;
  String valueStr = "";
  Map<dynamic, dynamic> valueJson = Map();
  int valueInt = 0;
  int valuePk = 0;
  late int resource;
  late FeatureTypeDesc typeDesc;
  late String extra;

  FeatureDesc.fromJson(Map<String, dynamic> json) {
    type = loadAndCheck(json, "type");
    typeDesc = lastState.featureTypesByID[type]!;
    name = loadAndCheck(json, "name");
    if (json["value"] is String) {
      updateStr(loadAndCheck(json, "value"));
    }
    if (json["value"] is int) {
      updateInt(json["value"]);
    }
    resource = json["resource"];
    extra = loadOrDefault(json, "extra", "");
    updateExtra();
  }

  FeatureDesc.fromUpdate(Map<String, dynamic> json) {
    type = json["feature"];
    typeDesc = lastState.featureTypesByID[type]!;
    name = typeDesc.name;
    if (json["value"] is String) {
      updateStr(loadAndCheck(json, "value"));
    }
    if (json["value"] is int) {
      updateInt(json["value"]);
    }
    resource = json["resource"];
    extra = loadOrDefault(json, "extra", "");
    updateExtra();
  }

  void updateInt(int i) {
    valueInt = i;
    valueStr = valueInt.toString();
  }

  void updateStr(String str) {
    valueStr = str;
    valueInt = int.tryParse(valueStr.trim()) ?? 0;
  }

  void updateExtra() {
    if (extra.contains("{") && extra.contains("}")) {
      valueJson = jsonDecode(extra);
    }
  }

  bool update(Map<String, dynamic> data) {
    print("update by $data");
    var value = data['value'];
    bool updated = false;
    String _extra = loadOrDefault(data, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }

    if (value == null) {
      print("no value in $data");
      return updated;
    }
    if (value is int) {
      if (valueInt != value) {
        updateInt(value);
        typeDesc.invalidateRelatedTransitions();
        return true;
      }
    }
    if (value is String) {
      if (valueStr != value) {
        updateStr(value);
        typeDesc.invalidateRelatedTransitions();
        return true;
      }
    }
    print("Unknown value type ${value.runtimeType.toString()}");
    return updated;
  }

  Future<void> set(String value) async {
    var rq = jsonEncode({"resource": resource, "value": value});
    await net.addToQueue("POST",
        ["character", lastState.char!.id.toString(), "set_feature"], rq);
  }

  Future<void> setJson() async {
    var rq = jsonEncode({"resource": resource, "extra": jsonEncode(valueJson)});
    await net.addToQueue("POST",
        ["character", lastState.char!.id.toString(), "set_feature_extra"], rq);
  }
}

class StatusHistoryDesc {
  String? status;
  DateTime? timestamp;
}

class CharacterDesc {
  late int id;
  late String name;
  List<FeatureDesc> features = [];
  Map<String, List<FeatureDesc>> featuresByTypeName = Map();
  Map<int, List<FeatureDesc>> featuresByTypeID = Map();
  Map<int, FeatureDesc> featureByResource = Map();
  late String location;
  late String currentStatus;
  List<StatusHistoryDesc> statusHistory = [];
  late DateTime lastModified;
  late String extra;
  Map<String, dynamic> extraJson = Map();

  CharacterDesc.fromJson(Map<String, dynamic> json) {
    id = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    if (json.containsKey("features")) {
      var jf = json["features"];
      jf.forEach((j) {
        var ft = FeatureDesc.fromJson(j);
        features.add(ft);
      });
    }
    currentStatus = loadOrDefault(json, "current_status", "UNKNOWN");
    lastModified = DateTime.parse(json["last_modified"] + "Z");
    extra = loadOrDefault(json, "extra", "");
    updateFeatureIndices();
  }

  void updateFeatureIndices() {
    featuresByTypeID.clear();
    featureByResource.clear();
    featuresByTypeName.clear();
    for (FeatureDesc ft in features) {
      if (!featuresByTypeName.containsKey(ft.name))
        featuresByTypeName[ft.name] = [];
      featuresByTypeName[ft.name]!.add(ft);
      if (!featuresByTypeID.containsKey(ft.type))
        featuresByTypeID[ft.type] = [];
      featuresByTypeID[ft.type]!.add(ft);
      featureByResource[ft.resource] = ft;
    }
    if (extra.contains("{") && extra.contains("}")) {
      extraJson = jsonDecode(extra);
    }
  }

  static Future<CharacterDesc> load(int id) async {
    var resp = await net.addToQueue("get", ["character", id.toString()], "");
    print(resp.response!.data);
    return CharacterDesc.fromJson(resp.response!.data);
  }

//{event: character, id: 1, name: char1, status: In game, location_id: null, player_id: 1, user_id: 2, last_modified: 2021-09-04 13:32:30}
  bool update(Map<String, dynamic> data) {
    bool updated = false;
    print("Update character by $data");
    String _name = loadOrDefault(data, "name", name);
    if (_name != name) {
      name = _name;
      updated = true;
    }

    String _currentStatus = loadOrDefault(data, "status", currentStatus);
    if (_currentStatus != currentStatus) {
      currentStatus = _currentStatus;
      updated = true;
    }
    /*  String _location=loadOrDefault(data, "location", location);
    if(_location!=location) {
      location = _location;
      updated=true;
    }
*/
    String _extra = loadOrDefault(data, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }
    DateTime _lastModified = DateTime.parse(data["last_modified"]);
    if (_lastModified != lastModified) {
      lastModified = _lastModified;
      updated = true;
    }
    return updated;
  }

  bool newer(CharacterDesc? other) {
    if (other == null) return true;
    return lastModified.compareTo(other.lastModified) < 0;
  }

  void _notifyByList(Iterable<int> lst, eventType et) {
    for (var i in lst) {
      var f = featureByResource[i];
      if (f == null) continue;
      if (f.typeDesc.tradable)
        notifyUserFeature(et, f.typeDesc);
      else
        notifyUser(eventType.CHARACTER_UPDATED);
    }
  }

  bool generateNotifyForDiff(CharacterDesc? other) {
    if (other == null) return true;
    var isChanged = false;
    var ourRes = featureByResource.keys.toSet();
    var otherRes = other.featureByResource.keys.toSet();
    var added = ourRes.difference(otherRes);
    var removed = otherRes.difference(ourRes);
    isChanged = added.isNotEmpty || removed.isNotEmpty;
    _notifyByList(added, eventType.INVENTORY_ADDED);
    other._notifyByList(removed, eventType.INVENTORY_REMOVED);

    for (var i in ourRes.intersection(otherRes)) {
      var ours = featureByResource[i]!;
      var others = other.featureByResource[i]!;
      if (ours.valueStr == others.valueStr) continue;
      isChanged = true;
      if (ours.typeDesc.tradable)
        notifyUserFeature(eventType.INVENTORY_UPDATED, ours.typeDesc);
      else
        notifyUser(eventType.CHARACTER_UPDATED);
      ours.typeDesc.invalidateRelatedTransitions();
      if (lastState.featureUpdateListeners.containsKey(ours.name))
        lastState.featureUpdateListeners[ours.name]!(ours);
    }
    return isChanged;
  }

  FeatureDesc? findFeatureByIntValue(String type, int id) {
    for (FeatureDesc c in featuresByTypeName[type] ?? []) {
      if (c.valueInt == id) return c;
    }
    return null;
  }

  FeatureDesc? findFeatureByStrValue(String type, String s) {
    for (FeatureDesc c in featuresByTypeName[type] ?? []) {
      if (c.valueStr == s) return c;
    }
    return null;
  }

  Future<void> setJson() async {
    var rq = jsonEncode({"extra": jsonEncode(extraJson)});
    await net.addToQueue(
        "POST", ["character", lastState.char!.id.toString(), "set_extra"], rq);
  }

  Future<int> newFeature(int ftype, String value, {String extra = ""}) async {
    var rq = jsonEncode({"type": ftype, "value": value, "extra": extra});
    Request resp = await net.addToQueue("POST",
        ["character", lastState.char!.id.toString(), "new_feature"], rq);
    return resp.response?.data["resource"] ?? 0;
  }

  FeatureDesc? checkResource(StateStore state, int resource) =>
      state.char!.featureByResource[resource];

  Future<FeatureDesc> waitForResource(int resource) {
    Completer<FeatureDesc> cmlp = Completer();
    StreamSubscription? subscription;
    var feat = checkResource(lastState, resource);
    if (feat != null) {
      cmlp.complete(feat);
    } else {
      subscription = stateStreamController.stream.listen((event) {
        feat = checkResource(event, resource);
        if (feat != null) {
          cmlp.complete(feat);
          subscription?.cancel();
          subscription = null;
        }
      });
    }
    return cmlp.future;
  }
}

class DealOperationDesc {
  late int pk;
  late int type;
  late int deal;
  late int buyer;
  late String operation;
  late int feature;
  late int amount;
  late int resource;

  late bool isBuyer;

  DealOperationDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    type = loadOrDefault(json, "type", -1);
    deal = loadAndCheck(json, "deal");
    buyer = loadAndCheck(json, "buyer");
    operation = loadAndCheck(json, "operation");
    feature = loadOrDefault(json, "feature", -1);
    amount = loadOrDefault(json, "amount", -1);
    resource = loadOrDefault(json, "resource", -1);
    isBuyer = lastState._char?.id == buyer;
  }

  Future<void> remove() async {
    var path = ["character", "deal-operations", pk.toString()];
    await net.addToQueue("delete", path, "");
  }

  Future<void> edit(int amount) async {
    Map<String, dynamic> json = Map();
    json["pk"] = pk;
    json["type"] = type;
    json["deal"] = deal;
    json["buyer"] = buyer;
    json["operation"] = operation;
    json["feature"] = feature;
    json["amount"] = amount;
    var path = ["character", "deal-operations", pk.toString()];
    await net.addToQueue("Put", path, jsonEncode(json));
  }

  bool update(Map<String, dynamic> json) {
    bool updated = false;
    int _amount = loadOrDefault(json, "amount", -1);
    if (_amount != amount) {
      amount = _amount;
      updated = true;
    }
    int _resource = loadOrDefault(json, "resource", -1);
    if (_resource != resource) {
      resource = _resource;
      updated = true;
    }
    int _feature = loadOrDefault(json, "feature", -1);
    if (feature != _feature) {
      feature = _feature;
      updated = true;
    }
    return updated;
  }

  bool compare(DealOperationDesc other) {
    if (amount != other.amount) return true;
    if (type != other.type) return true;
    if (buyer != other.buyer) return true;
    if (operation != other.operation) return true;
    if (feature != other.feature) return true;
    if (resource != other.resource) return true;

    return isBuyer != other.isBuyer;
  }
}

enum DealStatus { Created, Accepted, Failed }

class DealDesc {
  late int pk;
  late int initiator;
  late int acceptor;
  List<DealOperationDesc> operations = [];
  late bool initiator_accepted;
  late bool acceptor_accepted;
  late DealStatus status;
  late DateTime last_modified;

  late bool isWeInitiator;
  late bool isWeAcceptor;
  late bool isWeAccept;
  late bool isOtherAccept;
  late String extra;
  Map<String, dynamic> extraJson = Map();

  //Для быстрого доступа к опирациям с вещественными объектами
  Map<int, DealOperationDesc> operationsByRes = Map();

  //Для быстрого доступа к опирациям с количественными объектами(Например деньги)
  Map<int, DealOperationDesc> operationsByFeature = Map();

  get otherId {
    if (isWeAcceptor) return initiator;
    return acceptor;
  }

  DealDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    initiator = loadAndCheck(json, "initiator");
    acceptor = loadAndCheck(json, "acceptor");
    if (json["operations"] is List<dynamic>) {
      for (var js in json["operations"]) {
        var op = DealOperationDesc.fromJson(js);
        operations.add(op);
      }
    }
    initiator_accepted = loadAndCheck(json, "initiator_accepted");
    acceptor_accepted = loadAndCheck(json, "acceptor_accepted");
    parseStatus(json);
    last_modified = DateTime.parse(json["last_modified"]);
    extra = loadOrDefault(json, "extra", "");
    updateDealOperationIndices();
    updateAccept();
  }

  void updateDealOperationIndices() {
    operationsByFeature.clear();
    operationsByRes.clear();
    for (DealOperationDesc op in operations) {
      if (op.resource != -1) {
        operationsByRes[op.resource] = op;
      }
      if (op.feature != -1) operationsByFeature[op.feature] = op;
    }
    if (extra.contains("{") && extra.contains("}")) {
      extraJson = jsonDecode(extra);
    }
  }

  void updateAccept() {
    if (lastState._char!.id == initiator) {
      isWeInitiator = true;
      isWeAcceptor = false;
      isWeAccept = initiator_accepted;
      isOtherAccept = acceptor_accepted;
    }
    if (lastState._char!.id == acceptor) {
      isWeInitiator = false;
      isWeAcceptor = true;
      isWeAccept = acceptor_accepted;
      isOtherAccept = initiator_accepted;
    }
  }

  void parseStatus(Map<String, dynamic> json) {
    switch (json["status"]) {
      case "Created":
        status = DealStatus.Created;
        break;
      case "Failed":
        status = DealStatus.Failed;
        break;
      case "Accepted":
        status = DealStatus.Accepted;
        break;
      default:
        throw new Exception("ERROR: unknown error state ${json["status"]}");
    }
  }

  bool update(Map<String, dynamic> json) {
    bool updated = false;
    int _initiator = loadAndCheck(json, "initiator");
    if (_initiator != initiator) {
      initiator = _initiator;
      updated = true;
    }
    int _acceptor = loadAndCheck(json, "acceptor");
    if (acceptor != _acceptor) {
      acceptor = acceptor;
      updated = true;
    }

    bool _initiator_accepted = loadAndCheck(json, "initiator_accepted");
    if (_initiator_accepted != initiator_accepted) {
      initiator_accepted = _initiator_accepted;
      updated = true;
    }

    bool _acceptor_accepted = loadAndCheck(json, "acceptor_accepted");
    if (_acceptor_accepted != acceptor_accepted) {
      acceptor_accepted = _acceptor_accepted;
      updated = true;
    }

    String _extra = loadOrDefault(json, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }
    var _lastModified = last_modified;
    last_modified = DateTime.parse(json["last_modified"]);
    if (_lastModified != last_modified) updated = true;

    var _status = status;
    parseStatus(json);
    if (status != _status) updated = true;

    if (updated) {
      updateAccept();
      updateDealOperationIndices();
    }
    return updated;
  }

  bool newer(DealDesc other) {
    return last_modified.compareTo(other.last_modified) > 0;
  }

  Future<int> addOperation({int? resource, int? feature, int? amount}) async {
    if (resource == null && (feature == null || amount == null))
      throw Exception(
          "Invalid addDeal. resource or feature and amount should be not null");
    var json = Map<String, dynamic>();
    int buyer = 0;
    if (isWeInitiator)
      buyer = acceptor;
    else
      buyer = initiator;
    json["deal"] = pk;
    json["buyer"] = buyer;
    bool replicable =
        lastState.char?.featureByResource[resource]?.typeDesc.replicable ??
            false;
    if (replicable)
      json["operation"] = "Copy";
    else
      json["operation"] = "Trade";
    if (resource != null) json["resource"] = resource;
    if (feature != null) json["feature"] = feature;
    if (amount != null) json["amount"] = amount;
    Request rq = await net.addToQueue(
        "post", ["character", "deal-operations"], jsonEncode(json));
    print(rq.response?.data);

    if (rq.response!.data is Map<String, dynamic>) {
      return rq.response!.data["pk"] ?? -1;
    }

    return 0;
  }

  DealOperationDesc? getOperationById(int opID) {
    for (DealOperationDesc op in operations) {
      if (op.pk == opID) return op;
    }
    return null;
  }

  void updateDeal(PickResult result) {
    int? buyer = 0;
    if (isWeInitiator)
      buyer = acceptor;
    else
      buyer = initiator;
    for (int? i in result.checked) {
      var json = Map<String, dynamic>();
      json["deal"] = pk;
      json["buyer"] = buyer;
      json["operation"] = "Trade";
      json["resource"] = i;
      net
          .addToQueue(
              "post", ["character", "deal-operations"], jsonEncode(json))
          .then((r) {
        print(r.response!.data);
      });
    }
    for (var kv in result.amount.entries) {
      var json = Map<String, dynamic>();
      json["deal"] = pk;
      json["buyer"] = buyer;
      json["operation"] = "Trade";
      json["feature"] = kv.key;
      json["amount"] = kv.value;
      net
          .addToQueue(
              "post", ["character", "deal-operations"], jsonEncode(json))
          .then((r) {
        print(r.response!.data);
      });
    }
  }

  Future<void> accept() async {
    var json = Map<String, dynamic>();
    json["accept"] = true;
    var path = ["character", "deals", pk.toString()];
    await net.addToQueue("put", path, jsonEncode(json));
  }

  Future<void> updateExtra() async {
    if (extraJson.isNotEmpty) {
      extra = jsonEncode(extraJson);
    }
    var json = Map<String, dynamic>();
    json["accept"] = isWeAccept;
    json["extra"] = extra;
    var path = ["character", "deals", pk.toString()];
    await net.addToQueue("put", path, jsonEncode(json));
  }

  bool compare(DealDesc other) {
    if (pk != other.pk) return true;
    if (initiator != other.initiator) return true;
    if (acceptor != other.acceptor) return true;
    if (operations.length != other.operations.length) return true;
    if (initiator_accepted != other.initiator_accepted) return true;
    if (acceptor_accepted != other.acceptor_accepted) return true;
    if (status != other.status) return true;
    if (last_modified != other.last_modified) return true;
    for (var op in operations) {
      var otherop = other.getOperationById(op.pk);
      if (otherop == null || op.compare(otherop)) {
        return true;
      }
    }
    return false;
  }

  DealOperationDesc? getDealOpByPk(int oppk) {
    for (var op in operations) {
      if (op.pk == oppk) return op;
    }
    return null;
  }
}

class TreeElement<key, value> {
  Map<key, TreeElement<key, value>>? trunk;
  value? leaf;

  void put(List<key> path, value val) {
    if (path.length == 0) {
      leaf = val;
      return;
    }
    var next = path[0];
    if (trunk == null) trunk = SplayTreeMap();
    if (!trunk!.containsKey(next)) trunk![next] = TreeElement<key, value>();
    trunk![next]!.put(path.sublist(1), val);
  }

  TreeElement<key, value>? getElement(List<key> path) {
    if (path.length == 0) return this;
    if (trunk == null) return null;
    if (!trunk!.containsKey(path[0])) return null;
    return trunk![path[0]]!.getElement(path.sublist(1));
  }

  List<value> getList() {
    List<value> ret = [];
    if (leaf != null) ret.add(leaf!);
    if (trunk == null) return ret;
    for (var t in trunk!.values) {
      ret = ret + t.getList();
    }
    return ret;
  }

  value getMaximum() {
    if (trunk == null) return leaf!;
    return trunk!.values.last.getMaximum();
  }

  value? getMaximumCond(bool Function(value?) test) {
    for (var e in getList().reversed) {
      if (test(e)) return e;
    }
    return null;
  }

  value? getMinimum() {
    if (leaf != null) return leaf;
    return trunk!.values.first.getMaximum();
  }
}

class TransitionComponentDesc {
  late String title;
  late int feature_type;
  late String feature_type_name;
  late String feature_value;
  late bool any_value;
  bool? condition_passed;

  TransitionComponentDesc.fromJson(Map<String, dynamic> json) {
    title = loadAndCheck(json, "title");
    feature_type = loadOrDefault(json, "feature_type", -1);
    feature_type_name = loadOrDefault(json, "feature_type_name", "");
    feature_value = loadOrDefault(json, "feature_value", "");
    any_value = loadOrDefault(json, "any_value", false);
    condition_passed = loadOrDefault(json, "condition_passed", null);
  }

  void invalidate() {
    condition_passed = null;
  }
}

class TransitionDesc {
  late String key;
  late List<String> keySplit;
  int? level; //Часто бывает нужен
  String title = "";
  String description = "";
  late DateTime last_modified;
  List<TransitionComponentDesc> components = [];

  TransitionDesc.fromJson(Map<String, dynamic> json) {
    key = loadAndCheck(json, "key");
    keySplit = key.split("-");
    if (keySplit.last.contains('L')) {
      level = int.tryParse(keySplit.last.substring(1));
    }
    title = loadAndCheck(json, "title");
    description = loadAndCheck(json, "description");
    last_modified = DateTime.parse(json["last_modified"]);

    components = [];
    for (var c in json["components"]) {
      components.add(TransitionComponentDesc.fromJson(c));
    }
  }

  bool newer(TransitionDesc? other) {
    if (other == null) return true;
    return last_modified.compareTo(other.last_modified) > 0;
  }

  Future<TransitionDesc> load() {
    Completer<TransitionDesc> completer = Completer();
    List<String> path = ["character", "transitions", key];
    net.addToQueue("get", path, "").then((resp) {
      if (resp.response!.data != null) {
        Map<String, dynamic> json = resp.response!.data;
        if (json["components"] != null) {
          components.clear();
          for (var c in json["components"]) {
            var tc = TransitionComponentDesc.fromJson(c);
            components.add(tc);
            FeatureTypeDesc? ft = lastState.featureTypesByID[tc.feature_type];
            ft?.relatedTransitions.add(tc);
          }
          print("Components of $key updated");
          lastState.updated = true;
          completer.complete(this);
        }
      }
    });
    return completer.future;
  }

  bool get loaded {
    return components.map((c) => c.condition_passed).every((b) => b != null);
  }

  bool get passed {
    return components.map((c) => c.condition_passed).every((b) => b ?? false);
  }

  Future<void> apply() async {
    Map<String, dynamic> json = {"key": key};
    var path = ["character", "transitions", key, "apply"];
    print("Apply transition $key");
    await net.addToQueue("post", path, jsonEncode(json));
  }

  String getFailedCmp({String sep = ","}) {
    return components
        .where((e) => !(e.condition_passed ?? true))
        .map((e) => e.title)
        .join(sep);
  }
}

//TODO: функция гаратированного получения фичи
class StateStore {
  late Timer _timer, _timerRare;
  bool updated = false;
  PlayerDesc? _player;
  CharacterDesc? _char;
  int playerID = 0;

  Map<String, FeatureTypeDesc> featureTypesByName = Map();
  Map<int, FeatureTypeDesc> featureTypesByID = Map();
  Map<int, DealDesc> deals = Map();
  Map<int, CharacterDesc> _characters = Map();
  Map<String, TransitionDesc> transitions = Map();
  TreeElement<String, TransitionDesc> transTree = TreeElement();
  List<Error> errors = [];
  List<FeatureTypeDesc> globalListeners = [];
  BLEConnection? bleConnection;
  FeatureDesc? bleDeviceMAC;
  Map<String, FeatureUpdatedFunc> featureUpdateListeners = Map();
  bool autoTransitionInhibited = false;

  StateStore() {
    _timer = Timer.periodic(
        Duration(seconds: configuration.refreshRate), (tmx) => onTimer());
    _timerRare = Timer.periodic(Duration(minutes: 5), (timer) => onTimerRare());
  }

  Error? getError() {
    if (errors.isEmpty) return null;
    return errors[0];
  }

  void removeError(Error err) {
    errors.remove(err);
    stateStreamController.add(this);
  }

  void clearErrors() {
    errors.clear();
    stateStreamController.add(this);
  }

  void addError(Error e) {
    errors.insert(0, e);
    stateStreamController.add(this);
    print("ERROR: ${e.subsystem} ${e.code} ${e.message}");
  }

  Future<void> onTimerRare() async {
    await reloadFeatureTypes();
    await reloadTransitions();
    net.recheckWebsocket();
  }

  Future<void> onTimer() async {
    bool firstTime = false;
    var start = DateTime.now();
    if (!net.isReady) {
      print("onTimer canceled. Network not ready");
      return;
    }
    if (featureTypesByID.isEmpty && featureTypesByName.isEmpty) {
      await reloadFeatureTypes();
      firstTime = true;
    }
    if (_player == null) {
      if (playerID == 0) return;
      PlayerDesc p = await PlayerDesc.load(playerID);
      player = p;
      firstTime = true;
    }

    //CharacterDesc chr = await CharacterDesc.load(_player.character);
    if (_char == null || !net.webSocketReady) {
      await reloadCharacter();
    }

    if (!net.webSocketReady || firstTime) await reloadDeals();

    if (firstTime) await reloadTransitions();

    if (!net.webSocketReady) {
      for (var ft in globalListeners) {
        ft.reloadValues();
      }
    }
    if (configuration.blePeripherialMACFetature.isNotEmpty &&
        bleDeviceMAC == null) {
      FeatureTypeDesc? ft =
          featureTypesByName[configuration.blePeripherialMACFetature];
      if (ft != null) {
        FeatureDesc? f = char!.featuresByTypeName[ft.name]?.first;
        if (f == null) {
          TransitionDesc? tr = transitions["Get-${ft.name}"];
          if (tr == null) {
            print("No transition for get type ${ft.name}");
            //Отключаем попытки получить фичу так как нет транизшенa
            configuration.blePeripherialMACFetature = "";
          } else
            tr.apply();
        } else
          bleDeviceMAC = f;
      }
    }
    if (!autoTransitionInhibited) {
      for (String transname in configuration.autoTransitions) {
        TransitionDesc? trans = transitions[transname];
        if (trans == null) continue;
        if (!trans.loaded) {
          trans.load();
          continue;
        }
        if (trans.passed) {
          trans.apply();
          break; //1 транзишен за раз. Они могут быть связаны
        }
      }
    }
    //{ "type": "once",  "time": 300, "trans": "Decrease-health"}
    print(
        "Update done in ${DateTime.now().difference(start).inMilliseconds} ms");
  }

  Future<void> reloadAll() async {
    await reloadFeatureTypes();
    await reloadCharacter(forced: true);
    await reloadDeals(force: true);
    await reloadTransitions(force: true);
    net.recheckWebsocket();
  }

  Future<void> reloadTransitions({bool force = false}) async {
    var resp = await net.addToQueue("get", ["character", "transitions"], "");
    if (resp.response!.data is List<dynamic>) {
      for (var r in resp.response!.data) {
        var tran = TransitionDesc.fromJson(r);

        if (force || tran.newer(transitions[tran.key])) {
          print("Transtion ${tran.key} updated");
          transitions[tran.key] = tran;
          transTree.put(tran.keySplit, tran);
          updated = true;
        }
      }
      if (featureTypesByName.containsKey("Inited") &&
          transitions.containsKey("Init") &&
          !_char!.featuresByTypeName.containsKey("Inited")) {
        transitions['Init']!.apply();
      }
    }
  }

  Future<void> reloadDeals({bool force = false}) async {
    Request resp = await net.addToQueue("get", ["character", "deals"], "");
    if (resp.response!.data is List<dynamic>) {
      for (var r in resp.response!.data) {
        var deal = DealDesc.fromJson(r);
        if (deals.containsKey(deal.pk)) {
          if ((force && deal.compare(deals[deal.pk]!)) ||
              deal.newer(deals[deal.pk]!)) {
            print("Deal ${deal.pk} updated");
            notifyUser(eventType.DEAL_UPDATED);
            deals[deal.pk] = deal;
            updated = true;
          }
        } else {
          print("Deal ${deal.pk} added");
          deals[deal.pk] = deal;
          notifyUser(eventType.NEW_DEAL);
          updated = true;
        }
      }
    }
  }

  Future<Request> reloadCharacter({bool forced = false}) async {
    List<String> path = ["character", _player!.character.toString()];
    Map<String, dynamic> options = Map();

    Request resp = await net.addToQueue("get", path, "", options: options);
    CharacterDesc chr = CharacterDesc.fromJson(resp.response!.data);
    if (_char == null ||
        forced ||
        (chr.lastModified.compareTo(_char!.lastModified) > 0)) {
      if (chr.generateNotifyForDiff(char))
        notifyUser(eventType.CHARACTER_UPDATED);
      _char = chr;
      updated = true;
    }
    return resp;
  }

  Future<void> reloadFeatureTypes() async {
    Request resp = await net.addToQueue(
      "get",
      ["character", "feature-types"],
      "",
    );
    lastState.updateFeatureTypes(resp.response!.data);
  }

  //TODO: возможно надо распилить
  void update(Map<String, dynamic> data) {
    try {
      String event = data["event"];
      switch (event) {
        case "character":
          int charid = data["id"];
          print("update charid $charid");
          if (_char?.id == charid) {
            if (_char?.update(data) ?? false) {
              updated = true;
              notifyUser(eventType.CHARACTER_UPDATED);
            }
            return;
          }
          if (_characters.containsKey(charid)) {
            if (_characters[charid]?.update(data) ?? false) updated = true;
            return;
          }
          print("Unknown user id $charid");
          break;
        case "feature":
          print("update feature $data");
          int? character = data["character_id"];
          int res = data["resource"];
          if (character == null) //Global variable
          {
            int featureId = data["feature"];
            FeatureTypeDesc? ft = featureTypesByID[featureId];
            if (ft == null) {
              print("Global Feature type $featureId not found");
              reloadAll();
              return;
            }
            if (ft.updateValue(res.toString(), data["value"])) {
              updated = true;
              ft.invalidateRelatedTransitions();
            }
            print("Global feature ${ft.name} $res updated to ${data["value"]}");
            return;
          }
          if (!_char!.featureByResource.containsKey(res)) {
            FeatureDesc feature = FeatureDesc.fromUpdate(data);
            _char!.features.add(feature);
            _char!.updateFeatureIndices();
            notifyUserFeature(eventType.INVENTORY_UPDATED, feature.typeDesc);
            feature.typeDesc.invalidateRelatedTransitions();
            updated = true;
            return;
          }
          FeatureDesc feature = _char!.featureByResource[res]!;
          if (feature.update(data)) {
            notifyUserFeature(eventType.INVENTORY_UPDATED, feature.typeDesc);
            if (featureUpdateListeners.containsKey(feature.name))
              featureUpdateListeners[feature.name]!(feature);
            updated = true;
          }
          break;
        case "featureDelete":
          print("delete feature $data");
          int? character = data["character_id"];
          int res = data["resource"];
          if (character == null) //Global variable
          {
            int featureId = data["feature"];
            FeatureTypeDesc? ft = featureTypesByID[featureId];
            if (ft == null) {
              print("Global Feature type $featureId not found");
              reloadAll();
              return;
            }
            ft.values.remove(res.toString());
            print("Global feature ${ft.name} $res removed");
            return;
          }
          if (!_char!.featureByResource.containsKey(res)) {
            print("Resource $res not found $data reloading all");
            reloadAll();
            return;
          }
          FeatureDesc feature = _char!.featureByResource[res]!;
          _char!.features.remove(feature);
          _char!.updateFeatureIndices();
          feature.typeDesc.invalidateRelatedTransitions();
          updated = true;
          break;
        case "deal":
          print("Update deal $data");
          int dealID = data["pk"]!;
          if (!deals.containsKey(dealID)) {
            DealDesc d = DealDesc.fromJson(data);
            deals[dealID] = d;
            notifyUser(eventType.NEW_DEAL);
            updated = true;
            return;
          } else {
            DealDesc d = deals[dealID]!;
            if (d.update(data)) {
              notifyUser(eventType.DEAL_UPDATED);
              updated = true;
            }
          }
          break;
        case "dealOperation":
          print("Update deal operation $data");
          DealDesc deal = deals[data["deal"]!]!;
          int opID = data["pk"]!;
          DealOperationDesc? op = deal.getOperationById(opID);
          if (op == null) {
            DealOperationDesc o = DealOperationDesc.fromJson(data);
            deal.operations.add(o);
            notifyUser(eventType.DEAL_UPDATED);
            updated = true;
          } else {
            if (op.update(data)) {
              notifyUser(eventType.DEAL_UPDATED);
              updated = true;
            }
          }
          deal.updateDealOperationIndices();
          break;
        case "dealOperationDelete":
          print("Delete deal operation $data");
          DealDesc deal = deals[data["deal"]!]!;
          int opID = data["pk"]!;
          DealOperationDesc? op = deal.getOperationById(opID);
          if (op != null) {
            deal.operations.remove(op);
            deal.updateDealOperationIndices();
            notifyUser(eventType.DEAL_UPDATED);
            updated = true;
          }

          break;
        default:
          print("Unknown event type $event");
      }
    } on TypeError catch (e) {
      print("Wrong update message $data");
      print(e.stackTrace);
      print(e.toString());
    }
  }

  set player(PlayerDesc? plr) {
    if (_player == null || plr != _player) {
      _player = plr;
      updated = true;
    }
  }

  PlayerDesc? get player => _player;

  CharacterDesc? get char => _char;

  set char(CharacterDesc? d) {
    if (_char == null || (d!.lastModified.compareTo(_char!.lastModified)) > 0) {
      updated = true;
      _char = d;
    }
  }

  void checkUpdate() {
    if (updated) {
      updated = false;
      print("stateStream updated");
      stateStreamController.add(this);
    }
  }

  void updateFeatureTypes(List<dynamic> json) {
    json.forEach((ft) {
      if (!(ft is Map<String, dynamic>)) {
        print("wrong type $ft");
        return;
      }
      var f = FeatureTypeDesc.fromJson(ft);
      featureTypesByID[f.pk] = f;
      featureTypesByName[f.name] = f;
    });
  }

  void logout() {
    playerID = 0;

    _player = null;
    _char = null;
    deals.clear();
    _characters.clear();
    transitions.clear();
    transTree = TreeElement();
    stateStreamController.add(this);
    net.closeWebSocket();
  }

  void reset() {
    preferences!.remove("CurrentGame");
    logout();
  }

  void loadPrefsData() {
    playerID =
        preferences?.getInt(configuration.prefsPrefix + PLAYER_ID_KEY) ?? 0;
  }

  CharacterDesc? getCharacterById(int id) {
    var ret = _characters[id];
    if (ret == null) {
      CharacterDesc.load(id).then((loaded) {
        if (loaded.newer(_characters[id])) {
          _characters[id] = loaded;
          updated = true;
        }
      });
    }
    return ret;
  }

  Future<DealDesc> newDeal(int other) async {
    var json = Map<String, dynamic>();
    json["initiator"] = _char!.id;
    json["acceptor"] = other;
    var request = jsonEncode(json);
    var r = await net.addToQueue("post", ["character", "deals"], request);
    var deal = DealDesc.fromJson(r.response!.data);
    deals[deal.pk] = deal;
    updated = true;
    return deal;
  }

  Future<int> getHackDefensiveLevel(int target) async {
    var ret = await net.addToQueue("GET", ["hack", target.toString()], "");
    Map<String, dynamic> json = ret.response?.data as Map<String, dynamic>;
    int deflevel = loadAndCheck(json, "defensive_hack_level");
    int pk = loadAndCheck(json, "pk");
    if (pk != target) {
      print("Wrong pk number $deflevel and $pk");
      return -1;
    }
    return deflevel;
  }

  Future<List<FeatureDesc>> hackInspect(int target) async {
    List<FeatureDesc> ret = [];
    var req =
        await net.addToQueue("GET", ["hack", target.toString(), "inspect"], "");
    List<dynamic> items = req.response?.data;
    for (Map<String, dynamic> i in items) {
      ret.add(FeatureDesc.fromJson(i));
    }
    return ret;
  }

  //[{
  //     "buyer": <pk>,
  //     "operation": Trade|Copy,
  //     "resource": <pk>,
  //     "feature": <pk>,
  //     "amount": <int>
  // }]
  Future<bool> doHack(int target, PickResult pick) async {
    List<Map<String, dynamic>> request = [];

    for (var p in pick.checked) {
      Map<String, dynamic> m = Map();
      m["operation"] = "Trade";
      m["buyer"] = char!.id;
      m["resource"] = p;
      request.add(m);
    }
    for (var p in pick.amount.entries) {
      Map<String, dynamic> m = Map();
      m["operation"] = "Trade";
      m["buyer"] = char!.id;
      m["feature"] = p.key;
      m["amount"] = p.value;
      request.add(m);
    }
    var resp = await net.addToQueue(
        "POST", ["hack", target.toString(), "hack"], json.encode(request));
    Map<String, dynamic>? respjson = resp.response?.data;
    return respjson?["success"] ?? false;
  }
}

enum ErrorSubsystem {
  Unknown,
  Network,
  Bluetooth,
  System,
  NeedUpdate,
}

class Error {
  late String message;
  int? code;
  ErrorSubsystem? subsystem;

  Error() {
    message = "";
    code = 0;
    subsystem = ErrorSubsystem.Unknown;
  }

  Error.bluetooth(String m) {
    code = 0;
    message = m;
    subsystem = ErrorSubsystem.Bluetooth;
  }

  Error.system(String m) {
    code = 0;
    message = m;
    subsystem = ErrorSubsystem.System;
  }

  Error.network(int? c, String m) {
    code = c;
    message = m;
    subsystem = ErrorSubsystem.Network;
  }

  Error.needUpdate(int newversion) {
    code = newversion;
    message = "Необходимо обновление";
    subsystem = ErrorSubsystem.NeedUpdate;
  }
}
