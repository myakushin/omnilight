import 'package:flutter/material.dart';
import 'package:omnilight/main.dart';

import 'global.dart';
import 'model.dart';

enum InventoryMode { List, Pick }

class InventorySettings {
  InventoryMode mode = InventoryMode.List;
  Set<int> resourceFilter = Set();
  Set<int> featureFilter = Set();
  String label = configuration.getMenuElementName("/inventory") ?? "Инвентарь";
  List<FeatureDesc> features = lastState.char!.features;
  String addToDealButtonText = "Добавить в сделку";
}

class InventoryMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InventoryMenuState();
  }
}

class PickResult {
  final Set<int> checked = Set();
  final Map<int, int> amount = Map();
}

class InventoryPickMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InventoryPickState();
  }
}

class InventoryPickRequest {
  InventoryPickRequest(int? f, int? v) {
    feature = f;
    value = v;
  }

  int? feature;
  int? value = 0;
}

class InventoryPickState extends State<InventoryPickMenu> {
  final TextEditingController txtControl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    InventoryPickRequest? request =
        ModalRoute.of(context)!.settings.arguments as InventoryPickRequest?;
    if (request == null) {
      return Text("Не выбран желаемый элемент");
    }
    var ft = lastState.featureTypesByID[request.feature];
    if (ft == null) return Text("Нет такого элемента ${request.feature}");
    if (txtControl.text == "") txtControl.text = request.value.toString();

    return Scaffold(
      appBar: AppBar(
        title: Text("Укажите нужное количество"),
      ),
      body: ListView(
        children: <Widget>[
          Text("Выберете желаемое количество ${ft.name}"),
          TextField(
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              controller: txtControl),
          ElevatedButton(
            child: const Text("Сохранить"),
            onPressed: () {
              var ret = int.tryParse(txtControl.text);
              Navigator.of(context).pop(ret);
            },
          )
        ],
      ),
    );
  }
}

class InventoryMenuState extends State<InventoryMenu> {
  final PickResult pickResult = PickResult();

  Widget getPickInteger(BuildContext ctx, InventorySettings? settings,
      StateStore store, FeatureDesc ft, FeatureTypeDesc typeDesc) {
    if (!pickResult.amount.containsKey(ft.type)) pickResult.amount[ft.type] = 0;
    return ListTile(
      title: Text(typeDesc.name),
      subtitle: Text(
          "Всего: ${ft.valueInt} добавлено: ${pickResult.amount[ft.type]}"),
      trailing: IconButton(
        icon: const Icon(Icons.edit),
        onPressed: () {
          Navigator.of(ctx)
              .pushNamed("/inventory/pick",
                  arguments: InventoryPickRequest(ft.type, 0))
              .then((nw) {
            int? n = nw as int?;
            if (n != null)
              setState(() {
                pickResult.amount[ft.type] = n;
              });
          });
        },
      ),
    );
  }

  Widget getPickCheckbox(FeatureTypeDesc tp, FeatureDesc ft) {
    return CheckboxListTile(
        value: pickResult.checked.contains(ft.resource),
        onChanged: (n) {
          setState(() {
            if (n!)
              pickResult.checked.add(ft.resource);
            else
              pickResult.checked.remove(ft.resource);
          });
        },
        title: Text("${tp.name}: ${ft.valueStr}"));
  }

  Map<String, List<FeatureDesc>> sortFeatures(
      InventorySettings settings, StateStore state) {
    Map<String, List<FeatureDesc>> ret = Map();
    for (FeatureDesc feat in settings.features) {
      FeatureTypeDesc ft = feat.typeDesc;
      if (!ft.tradable) continue;
      if (ft.name.endsWith("Token")) continue;

      if (!ret.containsKey(ft.group)) ret[ft.group] = [];
      List<FeatureDesc> group = ret[ft.group]!;
      group.add(feat);
    }
    for (var group in ret.values) {
      group.sort((f1, f2) {
        var e1 = configuration.getInventoryElement(f1.name);
        var e2 = configuration.getInventoryElement(f2.name);
        return e1.getTitleText(f1, state).compareTo(e2.getTitleText(f2, state));
      });
    }
    return ret;
  }

  List<Widget> mapTiles(BuildContext context, InventorySettings settings,
      List<FeatureDesc> features, StateStore state) {
    List<Widget> ret = [];

    for (FeatureDesc feat in features) {
      FeatureTypeDesc ft = feat.typeDesc;
      if (settings.mode == InventoryMode.List) {
        ret.add(configuration
            .getInventoryElement(feat.name)
            .getListTile(context, feat, state));
      }
      if (settings.mode == InventoryMode.Pick) {
        if (settings.resourceFilter.contains(feat.resource)) continue;
        if (settings.featureFilter.contains(feat.type)) continue;
        if (ft.integer) {
          ret.add(getPickInteger(context, settings, state, feat, ft));
        } else {
          ret.add(getPickCheckbox(ft, feat));
        }
      }
    }

    return ret;
  }

  Widget? getBottom(InventorySettings settings, BuildContext context) {
    if (settings.mode == InventoryMode.Pick)
      return BottomAppBar(
          child: ElevatedButton(
              child: Text(settings.addToDealButtonText),
              onPressed: () {
                Navigator.of(context).pop(pickResult);
              }));
    if (settings.mode == InventoryMode.List)
      return BottomAppBar(
        child: ElevatedButton(
          child: Text("Создать файл"),
          onPressed: () async {
            FeatureDesc? feat =
                lastState.char!.findFeatureByStrValue("File", "--NEW-FILE--");
            if (feat == null) {
              TransitionDesc? trans = lastState.transitions["Get-File"];
              await trans?.apply();
              feat =
                  lastState.char!.findFeatureByStrValue("File", "--NEW-FILE--");
            }
            if (feat != null)
              Navigator.of(context)
                  .pushNamed("/inventory/details", arguments: feat);
          },
        ),
      );
    return null;
  }

  @override
  Widget build(BuildContext context) {
    InventorySettings settings =
        ModalRoute.of(context)!.settings.arguments as InventorySettings? ??
            InventorySettings(); //Load default settings
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");

          var features = sortFeatures(settings, snap.data!);

          return DefaultTabController(
              length: features.length,
              child: Scaffold(
                  appBar: AppBar(
                    title: Text(settings.label),
                    actions: [loadIndicatorBuilder()],
                    bottom: TabBar(
                        isScrollable: true,
                        tabs: features.keys
                            .map((name) => Tab(text: name))
                            .toList(growable: false)),
                  ),
                  bottomNavigationBar: getBottom(settings, context),
                  body: TabBarView(
                      children: features.values
                          .map((e) => ListView(
                              children:
                                  mapTiles(context, settings, e, snap.data!)))
                          .toList(growable: false))));
        });
  }
}

class InventoryDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FeatureDesc? feature =
        ModalRoute.of(context)!.settings.arguments as FeatureDesc?;

    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");
          if (feature == null) return Text("Нет объекта для отображения");
          var element =
              configuration.getInventoryElement(feature.typeDesc.name);
          return Scaffold(
            appBar: AppBar(
              title: Text("Инвентарь"),
              actions: [loadIndicatorBuilder()],
            ),
            body: element.getDetail(context, snap.data, feature),
            bottomNavigationBar: element.getBottom(ctx, feature),
          );
        });
  }
}

abstract class InventoryElementBase {
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text(feature.typeDesc.name),
        ),
        ListTile(
          title: Text(feature.valueStr),
        ),
      ],
    );
  }

  Widget getListTile(BuildContext ctx, FeatureDesc feature, StateStore state) {
    return ListTile(
      title: Text(getTitleText(feature, state)),
      onTap: () =>
          Navigator.of(ctx).pushNamed("/inventory/details", arguments: feature),
    );
  }

  String getTitleText(FeatureDesc feature, StateStore state) {
    return "";
  }

  Widget? getBottom(BuildContext ctx, FeatureDesc feature) {
    return null;
  }
}

class InventoryElementDefault extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    return "${feature.typeDesc.name}: ${feature.valueStr}";
  }
}

class InventoryElementReport extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    String display = feature.valueStr.trim();
    int len = display.indexOf("\n");
    if (len == -1) len = display.length;
    display = display.substring(0, len).trim();
    return "Отчет: $display";
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text("Отчет"),
        ),
        ListTile(
          title: Text(feature.valueStr.trim()),
        ),
      ],
    );
  }
}

class InventoryElementContact extends InventoryElementBase {
  Future<void> hack(BuildContext ctx, int targetid, int offlevel) async {
    int deflevel = await lastState.getHackDefensiveLevel(targetid);
    if (deflevel == -1) {
      lastState.addError(Error.system("Не удалось выяснить уровень защиты"));
      return;
    }
    if (deflevel > offlevel) {
      lastState.addError(Error.system("Недостаточный уровень для взлома"));
      return;
    }

    HackingRequest hr = HackingRequest.timeout((offlevel - deflevel) * 30);
    var miniGameObj =
        await Navigator.of(ctx).pushNamed("/hacking", arguments: hr);
    if (miniGameObj != HackingResult.DONE) {
      return;
    }
    List<FeatureDesc> items = await lastState.hackInspect(targetid);
    InventorySettings settings = InventorySettings();
    settings.features = items;
    settings.mode = InventoryMode.Pick;
    settings.label = "Инвентарь цели";
    settings.addToDealButtonText = "Украсть";
    var ret =
        await Navigator.of(ctx).pushNamed("/inventory", arguments: settings);
    PickResult? result = ret as PickResult?;
    if (result == null) return;
    bool hackres = await lastState.doHack(targetid, result);
    String restext = "";
    if (hackres)
      restext =
          "Взлом успешен. ${result.amount.length + result.checked.length} объектов украдено";
    else
      restext = "Не удалось переместить объекты";
    showDialog(
        context: ctx,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Результат взлома"),
            content: Text(restext),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("ok"))
            ],
          );
        });
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    List<Widget> buttons = [
      ListTile(
        title: Text(getTitleText(feature, state!)),
      )
    ];
    buttons.add(ElevatedButton(
      child: Text("Создать сделку"),
      onPressed: () {
        state.newDeal(feature.valueInt).then((deal) {
          Navigator.of(ctx).pushNamed("/trade/deal", arguments: deal.pk);
        });
      },
    ));
    int hoff =
        state.char!.featuresByTypeName["Hack-Offensive"]?.first.valueInt ?? -1;
    if (hoff > 0) {
      buttons.add(ElevatedButton(
          onPressed: () => hack(ctx, feature.valueInt, hoff),
          child: Text("Взломать")));
    }
    return ListView(children: buttons);
  }

  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    var id = feature.valueInt;
    var c = state.getCharacterById(id);
    if (c == null) return "Загрузка контакта...(id=$id)";
    return "Контакт с " + c.name;
  }
}

class InventoryElementProgram extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    int level = feature.valueJson["level"] ?? 0;
    return "Защитная прогремма $level уровня ";
  }

  Future<void> installToOther(TransitionDesc trans, BuildContext ctx) async {
    var other = await Navigator.of(ctx).pushNamed("/trade/new");
    FeatureDesc? otherFeat = other as FeatureDesc?;
    if (otherFeat == null) return;
    DealDesc deal = await lastState.newDeal(otherFeat.valueInt);
    lastState.autoTransitionInhibited = true;
    await trans.apply();
    FeatureDesc? token;
    int retry = 5;
    while (token == null) {
      token = lastState.char!.featuresByTypeName[trans.description]?.first;
      if (token == null) {
        await new Future.delayed(Duration(seconds: 1));
        if ((retry--) == 0) {
          lastState.autoTransitionInhibited = false;
          lastState
              .addError(Error.system("Не дождался токена от ${trans.key}"));
          return;
        }
      }
    }
    deal.addOperation(resource: token.resource);
    deal.accept();
    while (deal.status == DealStatus.Created) {
      await new Future.delayed(Duration(seconds: 1));
      if ((retry--) == 0) {
        lastState
            .addError(Error.system("Не дождался завершения сделки ${deal.pk}"));
      }
    }
    lastState.autoTransitionInhibited = false;
    Navigator.of(ctx).pop();
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    List<Widget> buttons = [];
    String transname = feature.valueJson["trans"] ?? "";
    TransitionDesc? trans = state!.transitions[transname];
    if (trans != null) {
      if (trans.loaded) {
        if (trans.passed) {
          buttons.add(ElevatedButton(
              onPressed: () async {
                await trans.apply();
                Navigator.of(ctx).pop();
              },
              child: Text("Установить себе")));
          buttons.add(ElevatedButton(
              onPressed: () => installToOther(trans, ctx),
              child: Text("Установить другому")));
        } else
          buttons.add(ListTile(
              title: Text("Не могу установить"),
              subtitle: Text(trans.getFailedCmp(sep: " "))));
      } else {
        buttons.add(CircularProgressIndicator());
        trans.load();
      }
    } else {
      buttons.add(ListTile(
        title: Text("Действий не обаружено"),
      ));
    }
    return ListView(children: buttons);
  }
}

class InventoryElementFile extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    String display = feature.valueStr.trim();
    int len = display.indexOf("\n");
    if (len == -1) len = display.length;
    display = display.substring(0, len).trim();
    return display;
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) =>
      InvElFileTextField(feature);

  @override
  Widget? getBottom(BuildContext ctx, FeatureDesc feature) {
    String authName = "";
    int? authid =
        feature.valueJson["fakeAuthor"] ?? feature.valueJson["realAuthor"];
    if (authid == null)
      authName = "Неизвестен";
    else {
      var achar = lastState.getCharacterById(authid);
      if (achar == null)
        authName = "Загрузка...";
      else
        authName = achar.name;
    }
    return BottomAppBar(
        child: ElevatedButton(
            child: Text("Автор:" + authName),
            onPressed: () => Navigator.of(ctx)
                .pushNamed("/inventory/signature", arguments: feature)));
  }
}

class InvElFileTextField extends StatefulWidget {
  final FeatureDesc target;

  InvElFileTextField(FeatureDesc _t) : target = _t;

  @override
  State<StatefulWidget> createState() {
    var ret = InvElFileTextFieldState();
    ret.setTarget(target);
    return ret;
  }
}

class InvElFileTextFieldState extends State<InvElFileTextField> {
  late FeatureDesc target;

  void setTarget(FeatureDesc t) {
    target = t;
    if (!target.valueJson.containsKey("realAuthor"))
      target.valueJson["realAuthor"] = lastState.char!.id;
  }

  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _controller.text = target.valueStr;
  }

  @override
  void dispose() {
    if (_controller.value.text != target.valueStr) {
      target.valueJson["realAuthor"] = lastState.char!.id;
      if (target.valueJson.containsKey("fakeAuthor"))
        target.valueJson.remove("fakeAuthor");
      target.set(_controller.value.text);
      target.setJson();
    }
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      expands: true,
      maxLines: null,
      minLines: null,
    );
  }
}

class FileSignature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FeatureDesc? feature =
        ModalRoute.of(context)!.settings.arguments as FeatureDesc?;

    String getNameById(int? authid) {
      if (authid == null)
        return "Неизвестен";
      else {
        var achar = lastState.getCharacterById(authid);
        if (achar == null)
          return "Загрузка...";
        else
          return achar.name;
      }
    }

    Future<void> makeFake(
        FeatureDesc feature, BuildContext ctx, int hacklevel) async {
      var other = await Navigator.of(ctx).pushNamed("/trade/new");
      FeatureDesc? otherFeat = other as FeatureDesc?;
      if (otherFeat == null) return;
      int otherId = otherFeat.valueInt;

      feature.valueJson["fakeAuthor"] = otherId;
      feature.valueJson["fakeLevel"] = hacklevel;
      feature.setJson();
    }

    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");
          if (feature == null) return Text("Нет объекта для отображения");
          List<Widget> widgets = [];
          widgets.add(ListTile(
              title: Text(
                  InventoryElementFile().getTitleText(feature, snap.data!))));
          bool fake = false;
          int hacklevel = snap.data!.char!.featuresByTypeName["Hack-Offensive"]
                  ?.first.valueInt ??
              -1;
          int checklevel = snap.data!.char!.featuresByTypeName["DocumentsCheck"]
                  ?.first.valueInt ??
              0;
          int? authid = feature.valueJson["realAuthor"];
          int? fakeid = feature.valueJson["fakeAuthor"];
          if (fakeid != null && authid != fakeid) {
            int level = feature.valueJson["fakeLevel"] ?? 0;
            if (checklevel >= level)
              fake = true;
            else
              authid = fakeid;
          }
          widgets.add(
              ListTile(title: Text("Автор:" + getNameById(fakeid ?? authid))));
          if (fake)
            widgets.add(ListTile(
                title: Text("Настоящий автор:" + getNameById(authid))));
          widgets.add(ListTile(
              title: Text("Статус:" + (fake ? "ПОДДЕЛЬНЫЙ" : "Подлинный"))));
          if (hacklevel > 0)
            widgets.add(ElevatedButton(
                onPressed: () => makeFake(feature, ctx, hacklevel),
                child: Text("Подделать")));
          return Scaffold(
              appBar: AppBar(
                title: Text("Свойства файла"),
                actions: [loadIndicatorBuilder()],
              ),
              body: ListView(
                children: widgets,
              ));
        });
  }
}
