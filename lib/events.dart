import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:omnilight/global.dart';
import 'package:omnilight/model.dart';

import 'blescanner.dart';

final FlutterLocalNotificationsPlugin notify =
    FlutterLocalNotificationsPlugin();

enum eventType {
  UNKNOWN,
  SYSTEM,
  CHARACTER_UPDATED,
  INVENTORY_UPDATED,
  INVENTORY_ADDED,
  INVENTORY_REMOVED,
  NEW_DEAL,
  DEAL_UPDATED,
}

var filteredEvents = Set();
var eventMessages = Map<eventType, String>();

void initEvents() {
  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('notify');
  final InitializationSettings initializationSettings =
      InitializationSettings(android: initializationSettingsAndroid);
  notify.initialize(initializationSettings);
  eventMessages[eventType.UNKNOWN] = "Неизвестное событие";
  eventMessages[eventType.SYSTEM] = "Системное событие";
  eventMessages[eventType.CHARACTER_UPDATED] =
      "Характеристики персонажа изменены";
  eventMessages[eventType.INVENTORY_UPDATED] = "Предмет в инвентаре изменен";
  eventMessages[eventType.INVENTORY_ADDED] = "Добавлено новое ";
  eventMessages[eventType.INVENTORY_REMOVED] = "Удалено ";
  eventMessages[eventType.NEW_DEAL] = "Новая сделка";
  eventMessages[eventType.DEAL_UPDATED] = "Сделка изменена";
}

void showNotification(String? text) {
  var android = AndroidNotificationDetails("Omnilight_0", "Omnilight");
  var details = NotificationDetails(android: android);
  notify.show(0, "Новое событие в игре", text, details);
}

void notifyUserCustom(eventType event, String? text) {
  if (filteredEvents.contains(event)) return;
  showNotification(text);
}

void notifyUser(eventType event) {
  notifyUserCustom(event, eventMessages[event]);
}

void notifyUserFeature(eventType event, FeatureTypeDesc ft) {
  notifyUserCustom(event, eventMessages[event]! + " " + ft.name);
}

void foregroudServiceFunc() {
  print("Hi from foregroud");
  if (configuration.bleConfig.enabled) startBLEScanning();
}

void checkForegroundService() async {
  await FlutterForegroundPlugin.setServiceMethodInterval(seconds: 10);
  await FlutterForegroundPlugin.setServiceMethod(foregroudServiceFunc);
  await FlutterForegroundPlugin.startForegroundService(
    holdWakeLock: false,
    onStarted: () {
      print("Foreground on Started");
    },
    onStopped: () {
      print("Foreground on Stopped");
    },
    title: "Omnilight",
    content: "Нажмите для перехода в приложение",
    iconName: "notify",
  );
}
