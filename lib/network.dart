import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:omnilight/config.dart';
import 'package:omnilight/global.dart';
import 'package:omnilight/model.dart';

enum RequestStatus { Queued, Runned, Failed, Done }
enum ResponseStatus {
  OK,
  NoConnectionToServer,
  ServerError,
  RequestError,
  UnknownError,
  InvalidLogin,
  BadCookie,
  BadTransition
}

typedef OnDoneFunction = void Function(Request);
typedef OnSuccessFunction = void Function();

class Request {
  Completer _completer = Completer<Request>();
  List<String> path = [];
  RequestStatus status = RequestStatus.Queued;
  ResponseStatus? responseStatus;
  String? respMessage;
  String? method;
  late DateTime startTime, endTime;
  Response? response;
  Headers? retHeaders;
  String? request;
  ServerConfig? server;
  int retries = 3;
  DioError? lastError;
  Map<String, dynamic> options = Map();
  List<int> goodCodes = [200];
  int dedupLevel = 0;

  get future => _completer.future;

  Uri getUri() {
    server = configuration.activeServer;
    String upath = "/api/";
    path.forEach((e) {
      upath = "$upath$e";
      if (!upath.endsWith("/")) upath = upath + "/";
    });
    return Uri.https(server!.path, upath, options);
  }

  void handleResponse(Response resp) {
    retHeaders = resp.headers;
    response = resp;
    endTime = DateTime.now();
    print(
        "Response to ${getUri()} in ${endTime.difference(startTime).inMilliseconds} ms");

    status = RequestStatus.Done;
    server!.doneOk();
  }

  void handleError(Object obj) {
    status = RequestStatus.Failed;
    server!.doneError();
    if (obj is DioError) {
      DioError resp = obj;
      if (goodCodes.contains(resp.response?.statusCode ?? 0)) {
        handleResponse(resp.response!);
        return;
      }
      print("Error");
      print(resp.error);
      lastError = resp;
      return;
    }
    print(obj);
  }

  void run(Dio client, Cookie? auth) {
    var opt = Options(method: method, contentType: "application/json");
    if (auth != null) opt.headers = {'Cookie': auth};
    var ft = client.requestUri(getUri(), data: request, options: opt);
    ft.then(this.handleResponse, onError: this.handleError);
    startTime = DateTime.now();
    status = RequestStatus.Runned;
  }

  void doOnDone() {
    _completer.complete(this);
  }

  void doOnError() {
    _completer.completeError(lastError!);
  }

  bool compare(Request other) {
    if (method != other.method) return false;
    if (request != other.request) return false;

    if (path.length != other.path.length) return false;

    for (int i = 0; i < path.length; i++) {
      if (path[i] != other.path[i]) return false;
    }
    if (options.length != other.options.length) return false;

    for (var o in options.entries) {
      if (other.options[o.key] != o.value) return false;
    }

    if (goodCodes.length != other.goodCodes.length) return false;

    for (int i = 0; i < goodCodes.length; i++) {
      if (goodCodes[i] != other.goodCodes[i]) return false;
    }

    return true;
  }
}

enum EnumNetworkStatus { IDLE, LOADING, ERROR, UNKNOWN }

class NetworkStatus {
  int packetsQueued = 0;
  EnumNetworkStatus status = EnumNetworkStatus.UNKNOWN;

  NetworkStatus();

  NetworkStatus.idle() {
    status = EnumNetworkStatus.IDLE;
  }

  NetworkStatus.error() {
    status = EnumNetworkStatus.ERROR;
  }

  NetworkStatus.loading(int queued) {
    status = EnumNetworkStatus.LOADING;
    packetsQueued = queued;
  }
}

enum WebSocketStatus { INIT, CONNECTING, ONLINE, FAILED }

class NetworkEngine {
  static const String AUTH_COOKIE_KEY = "_authcookie";

  var client = Dio();
  List<Request> requests = [];
  late Timer timer;
  Cookie? authCookie;
  bool _tryLoaded = false;
  StreamController status = StreamController<NetworkStatus>.broadcast();
  DateTime? lastProgramUpdateCheck;
  WebSocket? webSocket;
  WebSocketStatus webSocketStatus = WebSocketStatus.INIT;

  get webSocketReady => webSocketStatus == WebSocketStatus.ONLINE;

  NetworkEngine() {
    timer = Timer.periodic(Duration(microseconds: 100), processQueue);
    checkCookie();
  }

  void dismiss() {
    status.close();
  }

  void resetCookie() {
    preferences!.remove(configuration.prefsPrefix + AUTH_COOKIE_KEY);
    authCookie = null;
  }

  get isReady => authCookie != null;

  void checkCookie() {
    if (isReady) return;
    if (preferences != null && !_tryLoaded) {
      _tryLoaded = true;
      var cookie =
          preferences!.getString(configuration.prefsPrefix + AUTH_COOKIE_KEY);

      if (cookie != null) {
        authCookie = Cookie.fromSetCookieValue(cookie);
      }
    }
  }

  void processQueue(Timer tmr) {
    checkCookie();
    if (requests.isNotEmpty) {
      var r = requests.removeAt(0);
      switch (r.status) {
        case RequestStatus.Queued:
          r.run(client, authCookie);
          requests.add(r);
          break;
        case RequestStatus.Runned:
          requests.add(r);
          break;
        case RequestStatus.Done:
          r.doOnDone();
          break;
        case RequestStatus.Failed:
          if (r.retries > 0) {
            r.status = RequestStatus.Queued;
            r.retries--;
            requests.add(r);
          } else {
            String errors = r.lastError?.message ?? "";
            var data = r.lastError?.response?.data;
            List<dynamic> listErrors = [];

            if (data is Map<String, dynamic>) {
              var dr = data['errors'] ?? [];
              if (dr is List<dynamic>) {
                listErrors = dr;
              }
              if (dr is Map<String, dynamic>) {
                listErrors = dr.values.toList();
              }
            }
            if (data is List<dynamic>) listErrors = data;

            for (var e in listErrors) {
              if (e is String) {
                errors += e;
              }
              if (e is List<dynamic>) {
                errors += e.join(" ");
              }
            }
            addError(Error.network(r.lastError?.response?.statusCode,
                "${r.getUri().toString()} $errors"));
            r.doOnError();
          }
          break;
      }
    }
    lastState.checkUpdate();
    if (configuration.useWebSockets) {
      if (webSocketStatus == WebSocketStatus.INIT) {
        webSocketStatus = WebSocketStatus.CONNECTING;
        webSocketConnect().then((value) {
          webSocketStatus = WebSocketStatus.ONLINE;
          print("Web socket online");
        }, onError: (value) {
          print("WebSocket connection error $value");
          webSocketStatus = WebSocketStatus.FAILED;
          throw value;
        });
      }
    }
    if (requests.isNotEmpty) {
      status.add(NetworkStatus.loading(requests.length));
    } else {
      status.add(NetworkStatus.idle());
    }
    /*
    if (lastProgramUpdateCheck == null ||
        lastProgramUpdateCheck.difference(DateTime.now()).inSeconds > 300) {
      lastProgramUpdateCheck = DateTime.now();
      PackageInfo.fromPlatform().then((info) {
        int myBuildID = int.tryParse(info.buildNumber);
        String server = configuration.activeServer.path;
        String path = "static/client/version";
        client.requestUri(Uri.https(server, path)).then((resp) {
          if (resp.statusCode == 200) {
            var json = jsonDecode(resp.data);
            int newver = json['version'];
            print("Check update:New version $newver");
            if (newver > myBuildID) {
              lastState.addError(Error.needUpdate(newver));
            }
          }
        });
      });
    }*/
  }

  void recheckWebsocket() {
    if (configuration.useWebSockets &&
        webSocketStatus == WebSocketStatus.FAILED) {
      webSocketStatus = WebSocketStatus.INIT;
      print("Retry websocket connection");
    }
  }

  Future<void> webSocketConnect() async {
    Map<String, String> headers = {'Cookie': authCookie.toString()};
    webSocket = await WebSocket.connect(
        "wss://${configuration.activeServer.path}/ws/events",
        headers: headers);
    webSocket!
        .listen(webSocketData, onError: webSocketError, onDone: webSocketDone);
  }

  void webSocketError(dynamic data) {
    print("WS error $data ${data.runtimeType}");
    webSocketStatus = WebSocketStatus.FAILED;
  }

  void webSocketDone() {
    print("WebSocket done");
    webSocketStatus = WebSocketStatus.FAILED;
  }

  void webSocketData(dynamic data) {
    String? sData = data as String?;
    if (sData == null) {
      print("Wrong data type ${data.runtimeType.toString()} $data");
      return;
    }
    Map<String, dynamic> jData = json.decode(sData);
    Map<String, dynamic>? payload = jData["payload"];
    if (payload == null) {
      print("Wrong data from WebSocket: $data");
      return;
    }
    lastState.update(payload);
  }

  Future<Request> addToQueue(String action, List<String> path, String request,
      {Map<String, dynamic>? options, List<int>? good}) {
    var rq = Request();
    rq.method = action;
    rq.path = path;
    rq.request = request;
    if (good != null) rq.goodCodes.addAll(good);
    if (options != null) rq.options = options;
    for (var r in requests) {
      if (rq.compare(r)) {
        r.dedupLevel++;
        print(
            "Request $action to ${path.join("/")} deduplicated ${r.dedupLevel} times");
        return r.future;
      }
    }
    requests.add(rq);
    status.add(NetworkStatus.loading(requests.length));
    return rq.future;
  }

  Future<bool> requestCookie(int id, String password) async {
    String rq = jsonEncode({"pk": id, "password": password});
    var resp = await addToQueue("post", ["login"], rq);
    print(resp.response);
    var hmap = resp.response!.headers.map;
    if (hmap.containsKey("set-cookie")) {
      var sc = hmap['set-cookie']!;

      sc.forEach((cookie) {
        if (!cookie.contains("django_cyber_master_ssid")) return;
        var c = Cookie.fromSetCookieValue(cookie);
        authCookie = c;
        preferences!
            .setString(configuration.prefsPrefix + AUTH_COOKIE_KEY, cookie);
      });

      lastState.playerID = id;
      preferences!.setInt(
          configuration.prefsPrefix + PLAYER_ID_KEY, lastState.playerID);
      await closeWebSocket();
    }
    return true;
  }

  Future<void> closeWebSocket() async {
    if (configuration.useWebSockets &&
        webSocketStatus == WebSocketStatus.ONLINE) {
      await webSocket?.close();
      webSocketStatus = WebSocketStatus.INIT;
    }
  }

  Future<RegistrationAnswer> register(
      String name, String nickname, String charname, String medical) {
    var compl = Completer<RegistrationAnswer>();
    var rq = jsonEncode({
      "player": {
        "real_name": name,
        "nickname": nickname,
        "mediacal_info": medical
      },
      "character": {
        "name": charname,
      },
    });
    addToQueue("post", ["accounts", "register"], rq).then((resp) {
      var ans = RegistrationAnswer.fromJson(resp.response!.data);
      compl.complete(ans);
    });
    return compl.future;
  }
}

class RegistrationAnswer {
  late int pk;
  late String username;
  late String password;

  RegistrationAnswer.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    username = loadAndCheck(json, "username");
    password = loadAndCheck(json, "password");
  }
}
