import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:omnilight/config.dart';

import 'global.dart';
import 'inventory.dart';
import 'main.dart';
import 'model.dart';

class TradeMenu extends StatelessWidget {
  Widget selectIcon(DealDesc deal) {
    if (deal.isWeInitiator) return Icon(Icons.arrow_upward);
    return Icon(Icons.arrow_downward);
  }

  Widget iconCheck(bool check) {
    if (!check) return Icon(Icons.check_box_outline_blank);
    return Icon(Icons.check_box);
  }

  static String? getCharName(StateStore store, int id) {
    var c = store.getCharacterById(id);
    if (c == null) return "Загрузка...(id=$id)";
    return c.name;
  }

  static int dealPriority(DealDesc d) {
    if (d.status == DealStatus.Created) return 10;
    if (d.status == DealStatus.Accepted) return 5;
    if (d.status == DealStatus.Failed) return 1;
    return 0;
  }

  TextStyle? getTextStyle(DealDesc d) {
    if (d.status == DealStatus.Accepted)
      return const TextStyle(color: Colors.green);
    if (d.status == DealStatus.Failed)
      return const TextStyle(color: Colors.red);
    if (d.status == DealStatus.Created) return const TextStyle(color: null);
    return null;
  }

  List<Widget> listDeals(BuildContext context, StateStore state) {
    List<Widget> ret = [];
    List<DealDesc> deals = state.deals.values.toList();

    deals.sort((a, b) {
      int diff = dealPriority(b) - dealPriority(a);
      if (diff != 0) return diff;
      return b.last_modified.compareTo(a.last_modified);
    });

    for (var deal in deals) {
      if (deal.extraJson.containsKey("res")) {
        int pos = 0;
        for (var op in deal.operations) {
          if (op.resource != -1) pos += 1;
        }
        String text = "Заказ в рестаране";
        if (deal.isWeAcceptor)
          text = "Заказ от ${getCharName(state, deal.otherId)}";
        ret.add(ListTile(
          leading: selectIcon(deal),
          title: Text(text, style: getTextStyle(deal)),
          subtitle: Row(children: [
            Text("$pos позиций"),
          ]),
          onTap: () {
            Navigator.of(context).pushNamed("/trade/rest", arguments: deal.pk);
          },
        ));
      } else {
        ret.add(ListTile(
          leading: selectIcon(deal),
          title: Text("Сделка с ${getCharName(state, deal.otherId)}",
              style: getTextStyle(deal)),
          subtitle: Row(children: [
            Text("${deal.operations.length} позиций"),
            iconCheck(deal.isWeAccept),
            iconCheck(deal.isOtherAccept),
          ]),
          onTap: () {
            Navigator.of(context).pushNamed("/trade/deal", arguments: deal.pk);
          },
        ));
      }
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");

          return Scaffold(
            appBar: AppBar(
              actions: [loadIndicatorBuilder()],
              title: Text("Сделки"),
            ),
            body: ListView(
              children: listDeals(context, snap.data!),
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed("/trade/new").then((ret) {
                  if (ret is FeatureDesc) {
                    snap.data!.newDeal(ret.valueInt).then((nd) {
                      Navigator.of(context)
                          .pushNamed("/trade/deal", arguments: nd.pk);
                    });
                  }
                });
              },
            ),
          );
        });
  }
}

class DealMenu extends StatelessWidget {
  List<Widget> listItems(BuildContext ctx, StateStore state, DealDesc deal) {
    List<Widget> ret = [];
    for (var op in deal.operations) {
      FeatureDesc? ft;
      String txt = "";
      Widget? trailer;
      if (op.feature == -1) {
        ft = state.char!.featureByResource[op.resource];
        if (ft != null) {
          FeatureTypeDesc? ftype = state.featureTypesByID[ft.type];
          if (ftype?.expectsValue ?? false)
            txt = "${ft.valueStr}";
          else
            txt = ftype?.name ?? "Неизвестный объект ${ft.type}";
        } else
          txt = "Объект типа ${state.featureTypesByID[op.type]?.name}";
        trailer = IconButton(
          icon: Icon(Icons.delete_forever),
          onPressed: () {
            op.remove();
          },
        );
      } else {
        ft = state.char!.featuresByTypeID[op.feature]?.first;
        if (ft != null) {
          txt = "${op.amount} ${ft.name}";
          trailer = IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.of(ctx)
                  .pushNamed("/inventory/pick",
                      arguments: InventoryPickRequest(ft?.type, op.amount))
                  .then((nw) {
                int? n = nw as int?;
                if (n == null) return;
                if (n > 0)
                  op.edit(n);
                else
                  op.remove();
              });
            },
          );
        } else
          txt = "${op.amount} неизвестно чего";
      }
      Icon dir;
      if (op.isBuyer)
        dir = Icon(Icons.arrow_back);
      else
        dir = Icon(Icons.arrow_forward);

      ret.add(ListTile(
        leading: dir,
        title: Text(txt),
        trailing: trailer,
      ));
    }
    return ret;
  }

  Widget getOurAgree(StateStore state, DealDesc deal) {
    if (deal.isWeAccept)
      return RaisedButton(
        child: Text("Подтверждено"),
        color: Colors.green,
        onPressed: () {},
      );
    return RaisedButton(
      child: Text("Подтвердить"),
      onPressed: deal.accept,
      color: Colors.red,
    );
  }

  Widget getOtherAgree(StateStore state, DealDesc deal) {
    if (deal.isOtherAccept)
      return RaisedButton(
        child: Text("Подтверждено"),
        onPressed: () {},
        color: Colors.green,
      );
    return RaisedButton(
      child: Text("Не подтверждено"),
      onPressed: () {},
      color: Colors.red,
    );
  }

  void updateDeal(DealDesc deal, PickResult r) {
    for (var r in r.checked) {
      if (!deal.operationsByRes.containsKey(r)) {
        deal.addOperation(resource: r);
      }
    }
    for (var kv in r.amount.entries) {
      var f = deal.operationsByFeature[kv.key];
      if (f == null && kv.value != 0)
        deal.addOperation(feature: kv.key, amount: kv.value);
    }
  }

  @override
  Widget build(BuildContext context) {
    final int? dealId = ModalRoute.of(context)!.settings.arguments as int?;
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");
          var state = snap.data!;
          DealDesc? deal = state.deals[dealId];
          if (deal == null) return Text("Не могу найти сделку номер $dealId");
          return Scaffold(
            appBar: AppBar(
              actions: [loadIndicatorBuilder()],
              title: Text(
                  "Сделка c ${TradeMenu.getCharName(state, deal.otherId)}"),
            ),
            body: ListView(
              children: listItems(context, state, deal),
            ),
            bottomNavigationBar: BottomAppBar(
              //   shape: const CircularNotchedRectangle(),
              child: Container(
                height: 40,
                child: Row(
                  children: <Widget>[
                    getOurAgree(state, deal),
                    Expanded(
                        child: RaisedButton(
                      child: Icon(Icons.add),
                      //Только неподтвержденные сделки имеют кнопку +
                      onPressed: deal.status == DealStatus.Created
                          ? () {
                              var st = InventorySettings();
                              st.mode = InventoryMode.Pick;
                              for (var op in deal.operations) {
                                st.resourceFilter.add(op.resource);
                                st.featureFilter.add(op.feature);
                              }
                              Navigator.of(context)
                                  .pushNamed("/inventory", arguments: st)
                                  .then((r) {
                                if (r is PickResult) updateDeal(deal, r);
                              });
                            }
                          : null,
                    )),
                    getOtherAgree(state, deal)
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class NewDealMenu extends StatelessWidget {
  List<Widget> listItems(BuildContext context, StateStore? state) {
    List<Widget> ret = [];
    var contacts = state?.char?.featuresByTypeName["Contact"];
    if (contacts == null) {
      ret.add(ListTile(
          leading: Icon(Icons.not_interested),
          title: Text("Нет ни одного контакта")));
      return ret;
    }
    for (var ft in contacts) {
      ret.add(ListTile(
        leading: Icon(Icons.contacts),
        title: Text(TradeMenu.getCharName(state!, ft.valueInt)!),
        onTap: () {
          Navigator.of(context).pop(ft);
        },
      ));
    }

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return Text("Нет доступных данных");
          var state = snap.data;
          return Scaffold(
            appBar: AppBar(
              actions: [loadIndicatorBuilder()],
              title: Text("Выберете контрагента для новой сделки"),
            ),
            body: ListView(children: listItems(context, state)),
          );
        });
  }
}

class RestaurantDeal extends StatelessWidget {
  Widget errorScafold(String text) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Заказ в ресторане"),
      ),
      body: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    final int? dealId = ModalRoute.of(context)!.settings.arguments as int?;
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          var errorWidget = getErrorWidgetSnap(snap);
          if (errorWidget != null) return errorWidget;
          if (!snap.hasData) return errorScafold("Нет доступных данных");
          var state = snap.data!;
          DealDesc? deal = state.deals[dealId];
          if (deal == null)
            return errorScafold("Не могу найти сделку номер $dealId");
          if (deal.isWeInitiator && deal.status == DealStatus.Accepted)
            return errorScafold("Заказ выполнен. Детали уточняйте у ресторана");
          if (deal.isWeInitiator && deal.status == DealStatus.Failed)
            return errorScafold("Заказ не выполнен");
          List<MenuItemDesc> items = [];
          DealOperationDesc? creditsOp;
          int creditsCalculated = 0;
          Widget? bottom;
          for (var op in deal.operations) {
            FeatureDesc? f;
            if (op.resource != -1)
              f = state.char!.featureByResource[op.resource];
            if (op.feature != -1)
              f = state.char!.featuresByTypeID[op.feature]?.first;
            if (f == null) {
              print("Feature not found for res ${op.resource}(${op.feature}");
              continue;
            }
            if (f.name == "MenuItemToken") {
              if (f.valueStr.startsWith("--")) continue;
              if (f.valueJson.isEmpty) continue;
              MenuItemDesc m =
                  MenuItemDesc.fromJSON(f.valueJson as Map<String, dynamic>);
              m.feat = f;
              m.dealOp = op;
              items.add(m);
              creditsCalculated += m.cost;
            }
            if (f.name == "Кредиты") creditsOp = op;
          }
          TableDesc tableDesc = TableDesc.fromJSON(deal.extraJson);

          if (deal.status == DealStatus.Created) {
            if (creditsOp == null) {
              FeatureDesc? fcredis =
                  state.char!.featuresByTypeName["Кредиты"]?.first;
              if (fcredis != null && creditsCalculated != 0)
                deal.addOperation(
                    feature: fcredis.typeDesc.pk, amount: creditsCalculated);
            } else {
              if (creditsOp.amount !=
                  creditsCalculated) if (creditsCalculated == 0)
                creditsOp.remove();
              else
                creditsOp.edit(creditsCalculated);

              bottom = BottomAppBar(
                  child: ElevatedButton(
                      child: Text("Оплатить счет на ${creditsOp.amount} cr"),
                      onPressed: () {
                        deal.accept();
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      }));
            }
          } else {
            bottom = BottomAppBar(
                child: ElevatedButton(
                    child: Text("Итого ${creditsOp?.amount} cr"),
                    onPressed: null));
          }
          return Scaffold(
            appBar: AppBar(
              actions: [loadIndicatorBuilder()],
              title: Text("Заказ в ${tableDesc.restaurantName}"),
            ),
            body: ListView(
              children: items
                  .map((e) => ListTile(
                title: Text("${e.name} ${e.cost} cr"),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever),
                  onPressed: () => e.dealOp?.remove(),
                ),
              ))
                  .toList(),
            ),
            bottomNavigationBar: bottom,
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () => captureQrCode(context),
            ),
          );
        });
  }
}
