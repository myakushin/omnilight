import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'config.dart';
import 'load.dart';
import 'model.dart';
import 'network.dart';

const String PLAYER_ID_KEY = "_player_id";

late Configuration configuration;
late NetworkEngine net;

late StateStore lastState;

late StreamController<StateStore> stateStreamController;
SharedPreferences? preferences;
late LoadManager loadManager;

void addError(Error e) {
  lastState.addError(e);
  stateStreamController.add(lastState);
}

T loadAndCheck<T>(Map<String, dynamic> json, String name) {
  if (!json.containsKey(name)) throw FormatException("Key $name not found");
  var ret = json[name];
  if (ret is! T) throw FormatException("Key $name wrong type");
  return ret;
}

T loadOrDefault<T>(Map<String, dynamic> json, String name, T def) {
  if (!json.containsKey(name)) return def;
  var ret = json[name];
  if (ret is! T) return def;
  return ret;
}

Widget loadIndicatorBuilder() {
  return StreamBuilder<NetworkStatus>(
      initialData: NetworkStatus.idle(),
      stream: net.status.stream as Stream<NetworkStatus>?,
      builder: (BuildContext context, AsyncSnapshot<NetworkStatus> snapshot) {
        if (!snapshot.hasData) return SizedBox.shrink();
        if (snapshot.data!.status == EnumNetworkStatus.IDLE) {
          return IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                lastState.reloadAll();
              });
        }
        if (snapshot.data!.status == EnumNetworkStatus.LOADING) {
          return Stack(alignment: Alignment.center, children: [
            CircularProgressIndicator(),
            Text("${snapshot.data!.packetsQueued}")
          ]);
        }
        return SizedBox.shrink();
      });
}

Widget? getErrorWidgetSnap(AsyncSnapshot<StateStore?> state) =>
    getErrorWidget(state.data!);

Widget? getErrorWidget(StateStore state) {
  var error = state.getError();
  if (error == null) return null;
  if (error.subsystem == ErrorSubsystem.NeedUpdate) {
    return AlertDialog(
      title: Text("Необходимо обновление"),
      content: Text(
          "Даннное приложение было обновлено. Необходимо скачать новую версию ${error.code}."),
      actions: [
        TextButton(
          child: Text("Скачать"),
          onPressed: () {
            launch(
                "https://${configuration.activeServer.path}/static/client/${error.code}/app.apk");
            state.removeError(error);
          },
        ),
        TextButton(
          child: Text("Отмена"),
          onPressed: () {
            state.removeError(error);
          },
        )
      ],
    );
  }

  List<Widget> actions = [];
  actions.add(TextButton(
    child: Text("ok"),
    onPressed: () {
      state.removeError(error);
    },
  ));
  int errors = state.errors.length;
  if (errors > 1)
    actions.add(TextButton(
        onPressed: () {
          state.clearErrors();
        },
        child: Text("Закрыть все $errors ошибок ")));
  if (error.subsystem == ErrorSubsystem.Network && error.code == 403) {
    actions.insert(
        0,
        TextButton(
            onPressed: () {
              state.removeError(error);
              state.logout();
            },
            child: Text("Ввести")));
    return AlertDialog(
        title: Text("Ошибка авторизации"),
        content: Text(
            "Прозошла ошибка авторизации. Возможно нужно ввести пароль заново"),
        actions: actions);
  }
  return AlertDialog(
    title: Text("Ошибка ${error.code}"),
    content: Text(error.message),
    actions: actions,
  );
}
