import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:omnilight/global.dart';
import 'package:omnilight/main.dart';
import 'package:omnilight/model.dart';

import 'inventory.dart' as inventory;
import 'model.dart';
import 'trade.dart';

typedef void QRActionFunc(int id, BuildContext ctx);
typedef inventory.InventoryElementBase InvElementCreateFunc();
typedef QRTypeBase QrTypeCreate(Map<String, dynamic> json);
typedef CharPropertyBase CharPropCreate(
    String feature, Map<String, dynamic> json);

class QRAction {
  QRActionFunc? func;
  late String name;
  String? description;

  QRAction(String n, QRActionFunc? f)
      : func = f,
        name = n;

  QRAction.description(String d) {
    name = "1Описание";
    description = d;
  }
}

abstract class QRTypeBase {
  QRTypeBase(Map<String, dynamic> jsonArguments);

  void updateQResult(QRScannerResult res);
}

class MenuElement {
  late String label;
  late String menu;

  MenuElement.fromJson(Map<String, dynamic> json) {
    label = loadAndCheck<String>(json, "label");
    menu = loadAndCheck(json, 'menu');
  }
}

class ServerConfig {
  late String path;
  int ok = 0;
  int error = 0;
  bool needReselect = false;
  bool isActive = false;

  ServerConfig.fromJson(String json) {
    path = json;
  }

  void doneOk() {
    ok++;
  }

  void doneError() {
    error++;
  }
}

class BLEAction {
  late String name;
  List<String> args = [];

  BLEAction.fromString(String input) {
    var s = input.split("(");
    if (s.isEmpty) throw (FormatException("Wrong string $input"));
    name = s[0].trim();
    if (s.length > 1) {
      args = s[1].replaceAll(")", "").split(",");
    }
  }
}

class BLEShowTotal {
  late String feature;
  late String label;

  BLEShowTotal.fromJson(Map<String, dynamic> json) {
    feature = loadAndCheck(json, "feature");
    label = loadAndCheck(json, "label");
  }
}

enum BLE_ACTION_TRIGGER { onEnter, onExit, onTimer }

class BLEConfig {
  late bool enabled;
  late Map<String, String> uuid;
  Map<int, Map<BLE_ACTION_TRIGGER, List<BLEAction>>> beaconActions = Map();
  late int timeout;
  BLEShowTotal? showTotal;

  BLEConfig() {
    enabled = false;
    uuid = Map();
  }

  BLEConfig.fromJson(Map<String, dynamic> json) {
    enabled = loadAndCheck(json, 'enabled');
    timeout = loadOrDefault(json, 'timeout', 60);
    uuid = Map();
    Map<String, dynamic>? m = json['uuids'];
    if (m != null) {
      for (var kv in m.entries) {
        uuid[kv.key] = kv.value;
      }
    }
    Map<String, dynamic>? st = json['showTotal'];
    if (st != null) showTotal = BLEShowTotal.fromJson(st);
    Map<String, dynamic>? b = json['beacons'];
    if (b != null) {
      for (var kv in b.entries) {
        int? id = int.tryParse(kv.key);
        if (id == null) throw FormatException("Wrong number ${kv.key}");
        Map<String, dynamic> t = kv.value;

        for (var tkv in t.entries) {
          BLE_ACTION_TRIGGER trigger;
          switch (tkv.key) {
            case "onEnter":
              trigger = BLE_ACTION_TRIGGER.onEnter;
              break;
            case "onExit":
              trigger = BLE_ACTION_TRIGGER.onExit;
              break;
            case "onTimer":
              trigger = BLE_ACTION_TRIGGER.onTimer;
              break;
            default:
              throw FormatException("Wrong trigger ${tkv.key}");
          }
          List<dynamic> val = tkv.value;

          if (!beaconActions.containsKey(id)) beaconActions[id] = Map();
          if (!beaconActions[id]!.containsKey(trigger))
            beaconActions[id]![trigger] =
                val.map((e) => BLEAction.fromString(e)).toList();
        }
      }
    }
  }
}

enum CHAR_PROP_TYPES { INTEGER, SHOW_IF_EXIST, SELECTOR }

class CharPropertyAction {
  late String name;
  late String transition;
  late bool checkMaximum;

  CharPropertyAction.fromJson(Map<String, dynamic> json) {
    name = loadAndCheck(json, "name");
    transition = loadAndCheck(json, "transition");
    checkMaximum = loadOrDefault(json, "checkMaximum", false);
  }
}

abstract class CharPropertyBase implements Comparable<CharPropertyBase> {
  late CHAR_PROP_TYPES type;
  late String featureName;
  late String label;
  late int priority;

  CharPropertyBase.fromJson(String _feature, Map<String, dynamic> json) {
    featureName = _feature;
    label = loadOrDefault(json, "label", _feature);
    priority = loadOrDefault(json, "priority", 9999);
  }

  CharPropertyBase.undefined() {
    label = featureName = "Неизвестно";
  }

  Widget? getWidget(StateStore state, BuildContext ctx);

  @override
  int compareTo(CharPropertyBase other) {
    var c1 = priority.compareTo(other.priority);
    if (c1 == 0) return label.compareTo(other.label);
    return c1;
  }
}

class CharPropertyInteger extends CharPropertyBase {
  late String maximum;
  List<CharPropertyAction> actions = [];

  CharPropertyInteger.fromJson(String _name, Map<String, dynamic> json)
      : super.fromJson(_name, json) {
    type = CHAR_PROP_TYPES.INTEGER;

    maximum = loadOrDefault(json, "maximum", "");
    List<dynamic>? _actions = json['actions'];
    if (_actions != null) {
      for (var a in _actions) {
        actions.add(CharPropertyAction.fromJson(a));
      }
    }
  }

  String? generatePropText(CharacterDesc char) {
    var f = char.featuresByTypeName[featureName];
    var m = char.featuresByTypeName[maximum]?.first;
    int? value = 0;
    if (f?.first != null) value = f!.first.valueInt;
    String ret = "";
    if (m == null)
      ret = "$label $value";
    else {
      int maxvalue = m.valueInt;
      ret = "$label $value/$maxvalue";
    }
    return ret;
  }

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    String? text = generatePropText(state.char!);
    if (text == null) return null;
    List<Widget> buttons = [];
    for (var act in actions) {
      var trans = state.transitions[act.transition];
      if (trans == null) continue;
      bool maximumReached = false;
      if (act.checkMaximum) {
        //Получаем текущее и максимальное значение данной фичи
        int n =
            state.char!.featuresByTypeName[featureName]?.first.valueInt ?? 0;
        int m = state.char!.featuresByTypeName[maximum]?.first.valueInt ?? 0;
        if (n >= m) maximumReached = true;
      }
      if (!trans.loaded) {
        buttons.add(ElevatedButton(
          child: Text(act.name),
          onPressed: null,
        ));
        trans.load();
      } else if (trans.passed && !maximumReached)
        buttons.add(ElevatedButton(
            child: Text(act.name), onPressed: () => trans.apply()));
      else
        buttons.add(ElevatedButton(
          child: Text(act.name),
          onPressed: null,
        ));
    }

    return ListTile(
      title: Text(text),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: buttons,
      ),
    );
  }
}

class CharPropertyExist extends CharPropertyInteger {
  CharPropertyExist.fromJson(String _name, Map<String, dynamic> json)
      : super.fromJson(_name, json) {
    type = CHAR_PROP_TYPES.SHOW_IF_EXIST;
  }

  @override
  String? generatePropText(CharacterDesc char) {
    if (char.featuresByTypeName.containsKey(featureName)) return label;
    return null;
  }
}

class CharPropertySelector extends CharPropertyBase {
  late String transitionPrefix;
  String prerequisite = "";
  String prerequisiteMessage = "";

  CharPropertySelector.fromJson(String _name, Map<String, dynamic> json)
      : super.fromJson(_name, json) {
    type = CHAR_PROP_TYPES.SELECTOR;

    transitionPrefix = loadOrDefault(json, "transitionPrefix", "");
    prerequisite = loadOrDefault(json, "prerequisite", "");
    prerequisiteMessage = loadOrDefault(json, "prerequisiteMessage", "");
  }

  CharPropertySelector.undefined() : super.undefined() {
    transitionPrefix = "";
  }

  @override
  Widget getWidget(StateStore state, BuildContext ctx) {
    if (prerequisite != "" &&
        state.char!.featuresByTypeName[prerequisite] == null) {
      return ListTile(
        title: Text(label),
        subtitle: Text(prerequisiteMessage),
      );
    }
    var _featList = state.char!.featuresByTypeName[featureName];
    if (_featList == null) {
      return ListTile(
          title: Text(label),
          subtitle: Text("Не выбран(а)"),
          trailing: ElevatedButton(
              child: Text("Выбрать"),
              onPressed: () => Navigator.of(ctx)
                  .pushNamed("/character/selector", arguments: this)));
    } else {
      FeatureDesc feat = _featList.first;
      return ListTile(
        title: Text(label),
        subtitle: Text(feat.valueStr),
      );
    }
  }
}

class CharPropertyMenu extends CharPropertyBase {
  late String path;

  CharPropertyMenu.fromJson(String _name, Map<String, dynamic> json)
      : super.fromJson(_name, json) {
    path = loadAndCheck(json, "path");
  }

  @override
  Widget getWidget(StateStore state, BuildContext ctx) {
    return ElevatedButton(
        child: Text(label), onPressed: () => Navigator.of(ctx).pushNamed(path));
  }
}

class SkillConfiguration {
  late String classFeature;
  late String expFeature;
  late String transPrefix;
  late String crossClassFeature;
  late String crossTransPrefix;
  late String crossSkillGetPrefix;

  SkillConfiguration.fromJson(Map<String, dynamic> json) {
    classFeature = loadOrDefault(json, "ClassFeature", "");
    expFeature = loadOrDefault(json, "ExpFeature", "");
    transPrefix = loadOrDefault(json, "TransPrefix", "");
    crossClassFeature = loadOrDefault(json, "CrossClassFeature", "");
    crossTransPrefix = loadOrDefault(json, "CrossTransPrefix", "");
    crossSkillGetPrefix = loadOrDefault(json, "CCSkillPrefix", "");
  }

  SkillConfiguration() {
    classFeature = "";
    expFeature = "Experience";
    transPrefix = "GetSkill";
    crossSkillGetPrefix = crossTransPrefix = crossClassFeature = "";
  }
}

enum BLEPeripherialCharDirection { IN, OUT, BOTH }
enum BLEPeripheralCharType { INT, STRING }

class BLEPeripheralsServiceConf {
  late String uuid; //or not String?
  late String feature;
  late String label;
  late String transitionInc;
  late String transitionDec;
  late BLEPeripherialCharDirection direction;
  late BLEPeripheralCharType type;

  BLEPeripheralsServiceConf.fromJson(String _uuid, Map<String, dynamic> json) {
    uuid = _uuid;
    feature = loadOrDefault(json, "feature", "");
    label = loadOrDefault(json, "label", feature);
    transitionInc = loadOrDefault(json, "transitionInc", "");
    transitionDec = loadOrDefault(json, "transitionDec", "");
    String dir = json["direction"] ?? "(undefined)";
    dir = dir.toLowerCase();
    switch (dir) {
      case "in":
        direction = BLEPeripherialCharDirection.IN;
        break;
      case "out":
        direction = BLEPeripherialCharDirection.OUT;
        break;
      case "both":
        direction = BLEPeripherialCharDirection.BOTH;
        break;
      default:
        throw FormatException(
            "в описании службы BLE $uuid неправильно указан direction $dir");
    }
    String t = json["type"] ?? "(undefined)";
    t = t.toLowerCase();
    switch (t) {
      case "int":
        type = BLEPeripheralCharType.INT;
        break;
      case "str":
        type = BLEPeripheralCharType.STRING;
        break;
      default:
        throw FormatException(
            "в описании службы BLE $uuid неправильно указан type $dir");
    }
  }
}

class Configuration {
  int serial = 0;
  int refreshRate = 5;
  late String prefsPrefix;
  bool selfRegister = false;
  bool useWebSockets = false; //TODO: скорее всего должно стать true
  Map<String, QRTypeBase> qrActions = Map();
  late Map<String, inventory.InventoryElementBase> inventoryElements;
  late Map<String, InvElementCreateFunc> invCreate;
  late Map<String, QrTypeCreate> qrCreate;
  late List<ServerConfig> servers;
  ServerConfig? _activeServer;
  late List<MenuElement> menuElements;
  late BLEConfig bleConfig;
  List<CharPropertyBase> charProperty = [];
  late Map<String, CharPropCreate> charPropertyCreate;
  Map<String, Set<String>> inventoryGroups = Map();
  late SkillConfiguration skills;
  String blePeripheralsUUID = "";
  Map<String, BLEPeripheralsServiceConf> blePeripheralsService = Map();
  String blePeripherialMACFetature = "";
  List<String> autoTransitions = [];

  void prepareInventoryRelations() {
    invCreate = Map();
    invCreate["Default"] = () => new inventory.InventoryElementDefault();
    invCreate["Contact"] = () => new inventory.InventoryElementContact();
    invCreate["Report"] = () => new inventory.InventoryElementReport();
    invCreate["Program"] = () => new inventory.InventoryElementProgram();
    invCreate["File"] = () => new inventory.InventoryElementFile();

    qrCreate = Map();
    qrCreate["Player"] = (arg) => new QRPlayer(arg);
    qrCreate["CustomTransition"] = (arg) => new QRCustom(arg);
    qrCreate["Restaurant"] = (arg) => new QRRestaurant(arg);
    qrCreate["MenuItem"] = (arg) => new QRMenuItem(arg);

    charPropertyCreate = Map();
    charPropertyCreate["integer"] =
        (feature, json) => new CharPropertyInteger.fromJson(feature, json);
    charPropertyCreate["selector"] =
        (feature, json) => new CharPropertySelector.fromJson(feature, json);
    charPropertyCreate["menu"] =
        (feature, json) => new CharPropertyMenu.fromJson(feature, json);
    charPropertyCreate["exist"] =
        (feature, json) => new CharPropertyExist.fromJson(feature, json);
  }

  Configuration.testData() {
    prepareInventoryRelations();
    prefsPrefix = "con";
    qrActions = Map();
    inventoryElements = Map();
    inventoryElements["Default"] = invCreate["Default"]!();
    inventoryElements["Contact"] = invCreate["Contact"]!();
  }

  Configuration.fromJson(Map<String, dynamic> json) {
    prepareInventoryRelations();
    serial = loadAndCheck<int>(json, "serial");
    prefsPrefix = loadAndCheck<String>(json, 'prefix');
    refreshRate = loadOrDefault(json, "refreshRate", 3);
    selfRegister = loadOrDefault(json, "selfRegister", false);
    useWebSockets = loadOrDefault(json, "UseWebSockets", false);
    Map<String, dynamic>? q = json['qrAction'];
    if (q != null) {
      for (var i in q.entries) {
        String qrtype = i.value["actionType"] ?? "CustomTransition";
        if (!qrCreate.containsKey(qrtype))
          throw FormatException("Unknown QR code type $qrtype");
        qrActions[i.key] = qrCreate[qrtype]!(i.value);
      }
    }

    inventoryElements = Map();
    Map<String, dynamic>? inv = json['inventoryElements'];
    if (inv != null) {
      for (var i in inv.entries) {
        inventoryElements[i.key] = invCreate[i.value]!();
      }
    }

    menuElements = [];
    List<dynamic>? l = json['menu'];
    if (l != null) {
      for (Map<String, dynamic> e in l) {
        menuElements.add(MenuElement.fromJson(e));
      }
    }

    servers = [];
    List<dynamic>? s = json['servers'];
    if (s != null) {
      for (String srv in s) {
        servers.add(ServerConfig.fromJson(srv));
      }
    }
    Map<String, dynamic>? ble = json['blescanner'];
    if (ble != null)
      bleConfig = BLEConfig.fromJson(ble);
    else
      bleConfig = BLEConfig();
    Map<String, dynamic>? _cp = json['char-properties'];
    if (_cp != null) {
      charProperty = [];
      for (var p in _cp.entries) {
        var pname = p.key;
        String type = loadAndCheck(p.value, "type");
        var propCreate = charPropertyCreate[type];
        if (propCreate != null) {
          charProperty.add(propCreate(pname, p.value));
        } else
          throw Exception("Unknown property type $pname}");
      }
      charProperty.sort();
    }

    Map<String, dynamic>? _grp = json["inventoryGroups"];
    if (_grp != null) {
      for (var kv in _grp.entries) {
        List<dynamic> items = kv.value;
        inventoryGroups[kv.key] = Set<String>.from(items);
      }
    }
    Map<String, dynamic>? _skills = json["skills"];
    if (_skills != null)
      skills = SkillConfiguration.fromJson(_skills);
    else
      skills = SkillConfiguration();

    Map<String, dynamic>? _blep = json["BLEPeripherals"];
    if (_blep != null) {
      for (var bleserv in _blep.entries) {
        blePeripheralsUUID = bleserv.key;
        if (blePeripheralsUUID == "MACFeature") {
          blePeripherialMACFetature = bleserv.value;
          continue;
        }
        Map<String, dynamic> blechars = bleserv.value;
        for (var blechar in blechars.entries) {
          String uuid = blechar.key;
          blePeripheralsService[uuid] =
              BLEPeripheralsServiceConf.fromJson(uuid, blechar.value);
        }
      }
    }
    List<dynamic>? _auto = json["autotransitions"];
    if (_auto != null) {
      for (String t in _auto) {
        autoTransitions.add(t);
      }
    }
  }

  inventory.InventoryElementBase getInventoryElement(String? type) {
    inventory.InventoryElementBase? ret = inventoryElements[type!];
    if (ret == null) ret = inventoryElements["Default"];
    return ret!;
  }

  ServerConfig selectActiveServer() {
    _activeServer = servers[0];
    _activeServer!.isActive = true;
    return _activeServer!;
  }

  ServerConfig get activeServer {
    if (_activeServer == null) selectActiveServer();
    return _activeServer!;
  }

  String? getMenuElementName(String path) {
    for (var e in menuElements) {
      if (e.menu == path) return e.label;
    }
    return null;
  }
}

class QRPlayer extends QRTypeBase {
  late String createTransiton;
  StreamController<List<QRAction>>? stream;
  StreamSubscription? stateSubscription;

  QRPlayer(Map<String, dynamic> jsonArguments) : super(jsonArguments) {
    createTransiton = loadAndCheck(jsonArguments, "newContactTransition");
  }

  void dispose() {
    stream!.close();
  }

  void onStateUpdate(StateStore state, charid) {
    FeatureDesc? charFD =
        lastState.char!.findFeatureByIntValue("Contact", charid);
    List<QRAction> ret = [];
    if (charFD != null) {
      ret.add(QRAction(
          "${TradeMenu.getCharName(state, charid)} уже у вас в контактах. Нажмите для перехода",
          (playerid, ctx) {
        Navigator.of(ctx).pushNamed("/inventory/details", arguments: charFD);
      }));
    } else {
      ret.add(QRAction(
            "Добавить в контакты ${TradeMenu.getCharName(state, charid)} ",
            (id, ctx) {
              state.char!.newFeature(
            state.featureTypesByName["Contact"]!.pk, charid.toString());
      }));
    }
    TransitionDesc? heal = state.transitions['Get-Heal'];
    if (heal != null &&
        state.char!.findFeatureByStrValue("Навык", "Медицина1") != null) {
      if (heal.loaded) {
        if (heal.passed) {
          ret.add(QRAction("Лечить", (id, ctx) async {
            FeatureDesc? token =
                state.char!.featuresByTypeName["HealToken"]?.first;
            if (token == null) {
              await heal.apply();
              token = state.char!.featuresByTypeName["HealToken"]?.first;
              if (token == null) {
                throw Exception("Не удалось получить HealToken");
              }
            }
            DealDesc deal = await state.newDeal(charid);
            deal.addOperation(resource: token.resource);
            deal.accept();
          }));
        } else {
          ret.add(QRAction("Лечить [${heal.getFailedCmp()}]", null));
        }
      } else {
        heal.load();
      }
    }

    stream!.add(ret);
  }

  Stream<List<QRAction>> getActions(int id) {
    print("Action for player $id");
    PlayerDesc.load(id).then((player) {
      if (stateSubscription != null) stateSubscription!.cancel();
      stateSubscription = stateStreamController.stream
          .listen((state) => onStateUpdate(state, player.character));
      onStateUpdate(lastState, player.character);
    });
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "игрок";
  }
}

class QRCustom extends QRTypeBase {
  static const String reportFeatureName = "Report";
  StreamController<List<QRAction>>? stream;
  late String prefix;
  String? hackSuffix;
  String? descriptionSuffix;
  StreamSubscription? sub;

  QRCustom(Map<String, dynamic> jsonArguments) : super(jsonArguments) {
    prefix = loadAndCheck(jsonArguments, "prefix");
    hackSuffix = loadOrDefault(jsonArguments, "hackSuffix", null);
    descriptionSuffix = loadOrDefault(jsonArguments, "descriptionSuffix", null);
  }

  Widget successDialog(BuildContext context, TransitionDesc t) {
    return AlertDialog(
      title: Text("Выполнено"),
      content: Text(t.description),
      actions: [
        TextButton(
            child: const Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        TextButton(
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
            child: const Text("главное меню"))
      ],
    );
  }

  int getReportId(FeatureDesc ft) {
    var repid = ft.valueJson["ReportId"];
    if (repid is int) return repid;
    return int.parse(repid);
  }

  int getReportLevel(FeatureDesc ft) {
    var repid = ft.valueJson["ReportLevel"];
    if (repid is int) return repid;
    return int.parse(repid);
  }

  FeatureDesc? getMaxReportId(int id) {
    int maxReportLevel = -1;
    FeatureDesc? report;
    var reports = lastState.char!.featuresByTypeName[reportFeatureName];
    if (reports != null) {
      for (var ft in reports) {
        //if (ft.valueJson["Type"] != transitionPrefix) continue;

        if (getReportId(ft) != id) continue;

        if (ft.valueJson["ReportLevel"] > maxReportLevel) {
          maxReportLevel = ft.valueJson["ReportLevel"];
          report = ft;
        }
      }
    }
    return report;
  }

  QRAction defaultAction(TransitionDesc desc) =>
      QRAction(desc.title, (id, ctx) async {
        await desc.apply();
        if (desc.description.length > 0) {
          showDialog(
              context: ctx,
              builder: (BuildContext context) => successDialog(context, desc));
        }
      });

  QRAction scanAction(TransitionDesc desc) =>
      QRAction(desc.title, (id, ctx) async {
        FeatureDesc? report = getMaxReportId(id);
        if (report == null || getReportLevel(report) < desc.level!) {
          await desc.apply();
          report = getMaxReportId(id);
        }
        if (report != null) {
          Navigator.of(ctx).pushNamed("/inventory/details", arguments: report);
        } else {
          lastState.addError(Error.system("No report for ${desc.key}"));
        }
      });

  QRAction getActionByTree(TreeElement<String, TransitionDesc> tree) {
    TransitionDesc? desc = tree.getMaximumCond((c) => c!.passed);
    if (desc == null) {
      desc = tree.getMinimum();
      if (desc == null) {
        return QRAction.description("Действий не найдено");
      }
    }
    String suffix = desc.keySplit[2];
    if (!desc.passed) {
      if (suffix == descriptionSuffix)
        return QRAction.description("Описание недоступно");
      else {
        return QRAction("${desc.title} [${desc.getFailedCmp()}]", null);
      }
    }

    if (suffix == descriptionSuffix)
      return QRAction.description(desc.description);
    else if (suffix == hackSuffix) {
      return QRAction(desc.title, (id, ctx) {
        HackingRequest rq = HackingRequest(() {
          //TODO: переделать по новому
          desc!.apply();
          Navigator.of(ctx).pop();
        });
        Navigator.of(ctx).pushNamed("/hacking", arguments: rq);
      });
    } else if (suffix == "Scan")
      return scanAction(desc);
    else
      return defaultAction(desc);
  }

  List<QRAction> transformState(StateStore state, int id) {
    List<QRAction> ret = [];
    var trans = lastState.transTree.getElement([prefix, id.toString()]);
    if (trans == null) {
      return [QRAction("Нет действий с данным предметом", null)];
    }

    for (TreeElement<String, TransitionDesc> e in trans.trunk!.values) {
      List<Future<TransitionDesc>> loads = [];
      for (var l in e.getList()) {
        if (!l.loaded) loads.add(l.load());
      }
      if (loads.isEmpty)
        ret.add(getActionByTree(e));
      else
        ret.add(QRAction("Загрузка...", null));
    }
    ret.sort((a, b) => a.name.compareTo(b.name));
    return ret;
  }

  void updateStream(StateStore state, int id) =>
      stream!.add(transformState(state, id));

  void onListen(int id) {
    print("new Listener");
    if (sub != null) {
      sub!.cancel();
      sub = null;
    }
    sub =
        stateStreamController.stream.listen((state) => updateStream(state, id));
    updateStream(lastState, id);
  }

  void onCancel() {
    print("cancel");
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController(
        onListen: () => onListen(res.number!), onCancel: onCancel);

    res.actions = stream!.stream;
    //stateStreamController.stream
    //    .map((event) => transformState(event!, res.number!));
    lastState.updated = true;
    res.name = "";
    if (descriptionSuffix != null) {
      TransitionDesc? trans = lastState.transTree.getElement(
          [prefix, res.number.toString(), descriptionSuffix!])?.getMaximum();
      res.name = trans?.title ?? "<неизвестно>";
    }
  }
}

class TableDesc {
  late int table;
  late String restaurantName;
  late int charID;
  late TransitionDesc trans;
  late String origJson;

  TableDesc.fromTransition(TransitionDesc _t) {
    trans = _t;
    origJson = trans.description;
    Map<String, dynamic> js = json.decode(origJson);
    table = loadAndCheck(js, "table");
    restaurantName = loadAndCheck(js, "res");
    charID = loadAndCheck(js, "charid");
  }

  TableDesc.fromJSON(Map<String, dynamic> js) {
    table = loadAndCheck(js, "table");
    restaurantName = loadAndCheck(js, "res");
    charID = loadAndCheck(js, "charid");
  }
}

class MenuItemDesc {
  late TransitionDesc trans;
  late String name;
  late String restaurantName;
  late int cost;
  FeatureDesc? feat;
  DealOperationDesc? dealOp;

  MenuItemDesc.fromTransition(TransitionDesc _t) {
    trans = _t;
    name = trans.title;
    Map<String, dynamic> js = json.decode(trans.description);
    restaurantName = loadAndCheck(js, "res");
    cost = loadAndCheck(js, "cost");
  }

  MenuItemDesc.fromJSON(Map<String, dynamic> js) {
    restaurantName = loadAndCheck(js, "res");
    cost = loadAndCheck(js, "cost");
    name = loadAndCheck(js, "name");
  }

  String toJSON() {
    Map<String, dynamic> ret = Map();
    ret["res"] = restaurantName;
    ret["cost"] = cost;
    ret["name"] = name;
    return json.encode(ret);
  }
}

class QRRestaurant extends QRTypeBase {
  StreamController<List<QRAction>>? stream;

  void dismis() {
    stream?.close();
    stream = null;
  }

  QRRestaurant(Map<String, dynamic> jsonArguments) : super(jsonArguments);

  Future<void> makeOrder(int id, BuildContext ctx, TableDesc td) async {
    DealDesc order = await lastState.newDeal(td.charID);
    order.extra = td.origJson;
    await order.updateExtra();
    Navigator.of(ctx).pushNamed("/trade/rest", arguments: order.pk);
  }

  Stream<List<QRAction>> getActions(int id) {
    List<QRAction> ret = [];

    var trans = lastState.transTree
        .getElement(["Order", "Create", id.toString()])?.leaf;
    if (trans != null) {
      try {
        TableDesc table = TableDesc.fromTransition(trans);
        ret.add(QRAction.description(
            "Столик номер ${table.table} в ${table.restaurantName}"));
        DealDesc? deal = findDeal(table.restaurantName);
        if (deal == null)
          ret.add(QRAction(
              "Сделать заказ", (id, ctx) => this.makeOrder(id, ctx, table)));
        else {
          ret.add(QRAction(
              "Перейти к заказу",
              (id, ctx) => Navigator.of(ctx)
                  .pushNamed("/trade/rest", arguments: deal.pk)));
        }
      } on FormatException catch (e) {
        ret.add(QRAction.description("Ошибка декодирования ${e.message}"));
      }
    } else {
      ret.add(QRAction.description("Не найдено описание для стоилка $id"));
    }
    stream!.add(ret);
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "Столик";
  }
}

DealDesc? findDeal(String restaurantName) {
  for (var deal in lastState.deals.values) {
    //Завершенные нас не итересуют
    if (deal.status != DealStatus.Created) continue;
    if (deal.extraJson["res"] == restaurantName) return deal;
  }
  return null;
}

class QRMenuItem extends QRTypeBase {
  StreamController<List<QRAction>>? stream;

  QRMenuItem(Map<String, dynamic> jsonArguments) : super(jsonArguments);

  Future<void> addToOrder(int id, BuildContext ctx, MenuItemDesc item) async {
    int res = await lastState.char!.newFeature(
        lastState.featureTypesByName["MenuItemToken"]!.pk, "extra",
        extra: item.toJSON());
    await lastState.char!.waitForResource(res);
    DealDesc? deal = findDeal(item.restaurantName);
    if (deal == null) {
      lastState.addError(Error.system(
          "Не найден открытый счет. Возможно нужно снчала открыть счет через QR код стола"));
      Navigator.of(ctx).pop();
      return;
    }
    await deal.addOperation(resource: res);
    Navigator.of(ctx).pushReplacementNamed("/trade/rest", arguments: deal.pk);
  }

  Stream<List<QRAction>> getActions(int id) {
    List<QRAction> ret = [];
    var trans =
        lastState.transTree.getElement(["Order", "Item", id.toString()])?.leaf;
    if (trans != null) {
      try {
        MenuItemDesc item = MenuItemDesc.fromTransition(trans);
        ret.add(QRAction.description("${item.name} ${item.cost} cr"));
        ret.add(QRAction.description("в ресторане ${item.restaurantName}"));
        ret.add(QRAction(
            "Добавить в заказ", (id, ctx) => this.addToOrder(id, ctx, item)));
      } on FormatException catch (e) {
        ret.add(QRAction.description("Ощибка декодирования ${e.message}"));
      }
    } else {
      ret.add(QRAction.description("Не найдено описание для пункта меню $id"));
    }
    stream!.add(ret);
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "пункт меню";
  }
}
