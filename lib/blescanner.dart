import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'config.dart';
import 'global.dart';
import 'model.dart';

Timer? _BLETimer, _RefreshTimer;
bool needRefresh = false;
bool isScanning = false;
FlutterBlue flutterBlue = FlutterBlue.instance;
bool bleStarted = false;
//AssetsAudioPlayer audioPlayer;
const int RSSI_MEASURED_POWER = -61;

typedef void BLEActionFunc(List<String> args);

void startBLEScanning() {
  if (bleStarted) return;
  bleStarted = true;

  print("Start BLE Scanning");
//TODO: сделать регулиреумую скорость скана
  flutterBlue.state.first.then((value) {
    if (value == BluetoothState.on) {
      _BLETimer = Timer.periodic(Duration(seconds: 10), doScan);
      _RefreshTimer = Timer.periodic(Duration(seconds: 1), doRefresh);
    } else {
      lastState
          .addError(Error.bluetooth("Проблема с запуском Bluetooth $value "));
    }
  });

  flutterBlue.connectedDevices.then((devices) {
    for (BluetoothDevice dev in devices) {
      print("dev ${dev.id} connected");
    }
  });
  //bleManager.startPeripheralScan(]).listen(handeScanResult);
}

void doScan(Timer timer) {
  if (configuration.bleConfig.enabled) {
    print("Stopping BLE scan");
    flutterBlue.stopScan();
    //Андройд не сканирует простраство в фоновом режиме, если не фильтровать UUID сервисов.
    //мы их знаем из конфига, поэтому будем фильровать.
    List<Guid> uuids = configuration.bleConfig.beaconActions.keys
        .map((i) => Guid("$i-0000-1000-8000-00805F9B34FB".padLeft(36, "0")))
        .toList(growable: false);
    print("Starting BLE scan");
    //flutterBlue.startScan(withDevices: uuids, scanMode: ScanMode.lowLatency);
    flutterBlue.startScan(scanMode: ScanMode.lowLatency);
    flutterBlue.scanResults.listen(handelScanResult);
    print("Remove old devices");
    devices.removeWhere((key, value) {
      if (DateTime.now().difference(value.lastSeen).inSeconds >
          configuration.bleConfig.timeout) {
        value.beforeRemove();
        return true;
      }
      return false;
    });
  }
}

void doRefresh(Timer timer) {
  if (!needRefresh) return;
  List<KnownDevice> ours = [];
  for (var dev in devices.values) {
    if (dev.visible) ours.add(dev);
  }
  ourDevices.add(ours);
  needRefresh = false;
}
//TODO: рефактор под некоторое количество типов устройств
class KnownDevice {
  bool visible = false;
  String displayName = "Маяк";
  late BluetoothDevice dev;
  int? beaconID;
  String? action;
  late int rssi;
  DateTime? firstSeen;
  late DateTime lastSeen;
  Map<BLE_ACTION_TRIGGER, List<BLEAction>>? beaconActions;
  Map<String, BLEActionFunc> bleActionFuncs = Map();
  bool peripheral = false;

  double get distance {
    return pow(10, (RSSI_MEASURED_POWER - rssi) / (10 * 2)) as double;
  }

  void update(ScanResult res) {
    rssi = res.rssi;
    lastSeen = DateTime.now();
    print("Device ${dev.id} visible=$visible rssi=$rssi");
    if (visible) needRefresh = true;
  }

  KnownDevice.newFound(ScanResult res) {
    firstSeen = DateTime.now();
    dev = res.device;
    update(res);
    for (var svc in res.advertisementData.serviceUuids) {
      if (!svc.endsWith("1000-8000-00805f9b34fb")) continue;
      print(svc);
      beaconID = int.tryParse(svc.split("-")[0], radix: 16);
      if (beaconID == null) {
        print("can`t parse $svc for id");
        continue;
      }
      beaconActions = configuration.bleConfig.beaconActions[beaconID!];
      if (beaconActions == null) {
        print("BLE:no actions for id $beaconID");
        continue;
      }
      //bleActionFuncs["PlaySound"] = bleActionPlaySound;
      bleActionFuncs["ApplyTransition"] = bleActionApplyTransition;
      bleActionFuncs["Show"] = bleActionShow;
      runBeaconActions(BLE_ACTION_TRIGGER.onEnter);
    }
    if (res.device.name == "L4SHIELD") {
      peripheral = true;
      visible = true;
      displayName = "Щит";
      if (lastState.bleDeviceMAC?.valueStr == dev.id.id) {
        connect();
      }
    }
  }

  void runBeaconActions(BLE_ACTION_TRIGGER trigger) {
    for (BLEAction act in beaconActions?[trigger] ?? []) {
      var func = bleActionFuncs[act.name];
      if (func != null) {
        print("BLE:Start action ${act.name} for beacon ${dev.id} id $beaconID");
        func(act.args);
      } else {
        print(
            "BLE:Action ${act.name} for beacon ${dev.id} id $beaconID not found");
      }
    }
  }

  void beforeRemove() {
    runBeaconActions(BLE_ACTION_TRIGGER.onExit);
    print("Device ${dev.id} lost");
    if (visible) needRefresh = true;
  }

  void bleActionApplyTransition(List<String> args) {
    void _checkAndRun(TransitionDesc trans) {
      if (trans.passed) {
        print("BLE: Start transition ${trans.key}");
        trans.apply();
      } else {
        print("BLE: Transiton ${trans.key} not ready");
      }
    }

    for (var name in args) {
      var trans = lastState.transitions[name];
      if (trans == null) {
        print("Transition $name not found");
        return;
      }
      if (!trans.loaded) {
        trans.load().then(_checkAndRun);
      } else
        _checkAndRun(trans);
    }
  }

/*
  void bleActionPlaySound(List<String> args) {
    if (audioPlayer == null) audioPlayer = AssetsAudioPlayer();
    print("Load $args to playlist");
    var audios = args.map((e) => Audio(e)).toList();
    audioPlayer.open(Playlist(audios: audios)).then((value) {
      print("Start playing");
      audioPlayer.play();
    });
  }
*/
  void bleActionShow(List<String> args) {
    visible = true;
    if (args.isNotEmpty) displayName = args[0];
    needRefresh = true;
  }

  void connect() async {
    BluetoothDeviceState state = await dev.state.first;
    dev.state.listen(onConnectionChanged);
    if (state == BluetoothDeviceState.disconnected)
      await dev.connect(autoConnect: true);
  }

  void onConnectionChanged(BluetoothDeviceState state) {
    print("Device ${dev.id} connection state changed to ${state.toString()}");
    switch (state) {
      case BluetoothDeviceState.connected:
        dev.discoverServices().then(onServicesScaned);
        break;
      case BluetoothDeviceState.disconnected:
        lastState.bleConnection = null;
        lastState.updated = true;
        break;
      default:
        break;
    }
  }

  Future<void> onServicesScaned(List<BluetoothService> services) async {
    for (var s in services) {
      if (s.uuid.toString() == configuration.blePeripheralsUUID) {
        print("Found preperial!!!");
        lastState.bleConnection = BLEConnection(dev, s);
        if (lastState.bleDeviceMAC != null &&
            lastState.bleDeviceMAC?.valueStr != dev.id.id) {
          lastState.bleDeviceMAC!.set(dev.id.id);
        }
        await lastState.bleConnection!.setupAllCharacteristics();
      }
    }
  }
}

Map<DeviceIdentifier, KnownDevice> devices = Map();
StreamController<List<KnownDevice>> ourDevices = StreamController.broadcast();

void handelScanResult(List<ScanResult> results) {
  for (var result in results) {
    if (result.advertisementData.localName.startsWith("L4")) {
      var devid = result.device.id;
      if (devices.containsKey(devid))
        devices[devid]!.update(result);
      else
        devices[devid] = KnownDevice.newFound(result);
    }
  }
}

class ScanMenu extends StatelessWidget {
  Widget getLabel(BuildContext context) {
    var st = configuration.bleConfig.showTotal;
    if (st == null) return Text("Видимые метки");
    FeatureTypeDesc? ft = lastState.featureTypesByName[st.feature];
    if (ft == null) return Text("${st.feature} не найдена");
    return StreamBuilder(
        stream: ft.getUpdates(),
        builder: (BuildContext ctx, AsyncSnapshot<Map<String, dynamic>> data) {
          if (data.data == null) return Text("Нет данных");
          String val = "<неизвестно>";
          if (data.data!.isNotEmpty) val = data.data!.values.first.toString();
          return Text("${st.label} $val");
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: getLabel(context),
        ),
        body: StreamBuilder(
            initialData: List<KnownDevice>.empty(),
            stream: ourDevices.stream,
            builder: (BuildContext ctx, AsyncSnapshot<List<KnownDevice>> snap) {
              if (!snap.hasData) return Text("Нет доступных данных");
              List<Widget> tiles = [];
              for (KnownDevice dev in snap.data ?? []) {
                Widget? trail;
                if (dev.peripheral)
                  trail = ElevatedButton(
                      onPressed: () {
                        dev.connect();
                        Navigator.of(context).pop();
                      },
                      child: const Text("Соеденить"));
                tiles.add(ListTile(
                  title: Text(dev.displayName),
                  subtitle:
                      Text("расстояние ${dev.distance.toStringAsFixed(2)} м"),
                  trailing: trail,
                ));
              }
              return ListView(
                children: tiles,
              );
            }));
  }
}

class BLEConnectionLink {
  FeatureDesc? feat;
  late Guid uuid;
  BluetoothCharacteristic? blechar;
  late BLEPeripheralsServiceConf conf;
  int valueInt = 0;
  String valueStr = "";

  BLEConnectionLink(BLEPeripheralsServiceConf cfg) {
    feat = lastState.char!.featuresByTypeName[cfg.feature]?.first;
    if (feat != null ||
        [BLEPeripherialCharDirection.BOTH, BLEPeripherialCharDirection.OUT]
            .contains(cfg.direction))
      lastState.featureUpdateListeners[cfg.feature] = onFeatureUpdated;
    uuid = Guid(cfg.uuid);
    conf = cfg;
  }

  Future<void> onFeatureUpdated(FeatureDesc ft) async {
    feat = ft;
    if (blechar == null) return;
    print("Update char ${blechar!.uuid} to ${ft.valueInt}");
    if (conf.type == BLEPeripheralCharType.INT) {
      await blechar!.write([feat!.valueInt]);
    }
    if (conf.type == BLEPeripheralCharType.STRING) {
      await blechar!.write(feat!.valueStr.codeUnits);
    }
  }

  void connectChar(BluetoothCharacteristic c) {
    blechar = c;
  }

  Future<void> syncBLE() async {
    print("Syncing ${blechar!.uuid} ${conf.direction}");
    if (conf.direction == BLEPeripherialCharDirection.IN ||
        conf.direction == BLEPeripherialCharDirection.BOTH)
      onData(await blechar!.read());
    if (conf.direction == BLEPeripherialCharDirection.OUT && feat != null)
      await onFeatureUpdated(feat!);
  }

  void onData(List<int> data) {
    print("new data for  ${blechar!.uuid} as $data");
    if (conf.type == BLEPeripheralCharType.INT) {
      valueInt = data[0];
      valueStr = valueInt.toString();
    }
    if (conf.type == BLEPeripheralCharType.STRING) {
      valueInt = -1;
      valueStr = String.fromCharCodes(data);
    }
    if (feat != null &&
        [BLEPeripherialCharDirection.IN, BLEPeripherialCharDirection.BOTH]
            .contains(conf.direction)) {
      feat!.set(valueStr);
    }
    lastState.updated = true;
  }

  Future<void> setupNotify() async {
    if (!blechar!.properties.notify) return;
    if (blechar!.isNotifying) return;
    await blechar!.setNotifyValue(true);
    blechar!.value.listen(onData);
  }
}

class BLEConnection {
  late BluetoothDevice dev;
  List<BLEConnectionLink> links = [];

  BLEConnection(BluetoothDevice _d, BluetoothService serv) {
    dev = _d;
    for (var ch in configuration.blePeripheralsService.values) {
      links.add(BLEConnectionLink(ch));
    }
    for (var ch in serv.characteristics) {
      BLEConnectionLink? link = getLinkByUUID(ch.uuid);
      if (link != null) {
        link.connectChar(ch);
      } else
        print("Unknown characteristic ${ch.uuid}");
    }
  }

  Future<void> setupAllCharacteristics() async {
    for (var l in links) {
      await l.syncBLE();
      await l.setupNotify();
    }
  }

  BLEConnectionLink? getLinkByUUID(Guid uuid) {
    for (var l in links) {
      if (l.uuid == uuid) return l;
    }
    return null;
  }
}
