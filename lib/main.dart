import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:omnilight/load.dart';
import 'package:omnilight/model.dart';
import 'package:omnilight/network.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qrcode_forked/qrcode_forked.dart';

import 'blescanner.dart';
import 'config.dart';
import 'events.dart';
import 'global.dart';
import 'inventory.dart';
import 'trade.dart';

void main() {
  loadManager = LoadManager();
  loadManager.startLoading();
  initEvents();
  //net = NetworkEngine();
  //configuration = Configuration.testData();
  //stateStreamController = StreamController.broadcast();
  //lastState = StateStore();
  //stateStreamController.add(lastState);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Permission> perms = [
      Permission.camera,
      Permission.notification,
//      Permission.ignoreBatteryOptimizations,
      Permission.locationWhenInUse,
//      Permission.locationAlways,
    ];
    perms.request().then((value) {
      for (var kv in value.entries) {
        if (!kv.value.isGranted) {
          lastState.addError(Error.system(
              "Для корректной работы необходимо разрешние ${kv.key}. Но оно имеет статус ${kv.value} "));
        }
      }
    });
    /*  Permission.locationAlways.serviceStatus.then((value) {
      if (value.isDisabled)
        lastState.addError(Error.system(
            "Для корректной работы необходимо разрешние доступа к местоположению Но оно имеет статус $value "));
    });*/
    return MaterialApp(
      title: "Omnilight",
      theme: ThemeData.dark(),
      home: OmniHome(),
      routes: <String, WidgetBuilder>{
        "/login": (BuildContext ctx) => LoginMenu(),
        "/register": (BuildContext ctx) => RegistrationMenu(),
        "/charmenu": (BuildContext ctx) => CharacterMenu(),
        "/character/selector": (BuildContext ctx) => Selector(),
        "/character/crossclass": (BuildContext ctx) => SelectCrossClass(),
        "/inventory": (BuildContext ctx) => InventoryMenu(),
        //Restures: PickResult
        "/inventory/pick": (BuildContext ctx) => InventoryPickMenu(),
        //Argument: FeatureDesc
        "/inventory/details": (BuildContext ctx) => InventoryDetails(),
        "/inventory/signature": (BuildContext ctx) => FileSignature(),
        "/trade": (BuildContext ctx) => TradeMenu(),
        "/trade/deal": (BuildContext ctx) => DealMenu(),
        "/trade/new": (BuildContext ctx) => NewDealMenu(),
        "/trade/rest": (BuildContext ctx) => RestaurantDeal(),
        "/qrscanner": (BuildContext ctx) => QRScanner(),
        "/qractions": (BuildContext ctx) => QRActionMenu(),
        "/blescan": (BuildContext ctx) => ScanMenu(),
        "/settings": (BuildContext ctx) => SettingsWidget(),
        "/hacking": (BuildContext ctx) => Hacking(),
        "/skills": (BuildContext ctx) => Skills(),
        "/bleperipherals": (BuildContext ctx) => BLEPeripheralsMenu()
      },
    );
  }
}

typedef MainMenuElementCheck = bool Function(StateStore? state);

class MainMenuElement {
  String? path;
  String? label;
  MainMenuElementCheck? checkFunc;

  MainMenuElement(p, l, {MainMenuElementCheck? onCheck}) {
    path = p;
    label = l;
    checkFunc = onCheck;
  }

  bool check(StateStore? st) {
    if (checkFunc == null) return true;
    return checkFunc!(st);
  }
}

void captureQrCode(BuildContext context) {
  Navigator.of(context).pushNamed("/qrscanner").then((val) {
    QRScannerResult? res = val as QRScannerResult?;
    if (res == null) {
      print("QRCode scanner return no data");
      return;
    }
    configuration.qrActions[res.code!]?.updateQResult(res);
    Navigator.of(context).pushNamed("/qractions", arguments: res);
  });
}

class OmniHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    checkForegroundService();
    List<MainMenuElement> elements = [];
    elements.add(MainMenuElement("/login", "Вход", onCheck: (st) {
      return st!.char == null;
    }));
    elements.add(MainMenuElement("/register", "Регистрация", onCheck: (st) {
      return st!.char == null && configuration.selfRegister;
    }));
    for (var e in configuration.menuElements) {
      elements.add(MainMenuElement(e.menu, e.label, onCheck: (st) {
        return st!.char != null;
      }));
    }

    return StreamBuilder<StateStore?>(
        initialData: lastState,
        stream: stateStreamController.stream,
        builder: (BuildContext context, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];
          elements.forEach((e) {
            if (e.check(shot.data)) {
              widgets.add(ListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed(e.path!);
                  },
                  title: Text(e.label!)));
            }
          });
          return getErrorWidgetSnap(shot) ??
              Scaffold(
                  appBar: AppBar(
                    title: Text("Возможные действия"),
                    actions: <Widget>[
                      loadIndicatorBuilder(),
                      IconButton(
                          icon: Icon(
                            Icons.camera_alt,
                          ),
                          onPressed: () => captureQrCode(context)),
                      IconButton(
                        icon: Icon(Icons.settings),
                        onPressed: () {
                          Navigator.of(context).pushNamed("/settings");
                        },
                      )
                    ],
                  ),
                  body: ListView(children: widgets));
        });
  }
}

class LoginMenu extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _loginController = TextEditingController();
  final _pwController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final RegistrationAnswer? answer =
        ModalRoute.of(context)!.settings.arguments as RegistrationAnswer?;
    if (answer != null) {
      _loginController.text = answer.pk.toString();
      _pwController.text = answer.password;
    }
    return Scaffold(
        appBar: AppBar(title: Text("Введите данные для подключения к серверу")),
        body: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: _loginController,
                  decoration: const InputDecoration(
                      hintText: "Введите ID пользователя"),
                ),
                TextFormField(
                  decoration: const InputDecoration(hintText: "Введите пароль"),
                  obscureText: answer == null,
                  controller: _pwController,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: StreamBuilder<StateStore?>(
                      initialData: lastState,
                      stream: stateStreamController.stream,
                      builder:
                          (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
                            return getErrorWidgetSnap(state) ??
                            ElevatedButton(
                                child: Text("Войти"),
                                onPressed: () async {
                                  if (!_formKey.currentState!.validate())
                                    return;
                                  await net.requestCookie(
                                      int.parse(_loginController.text),
                                      _pwController.text);
                                  Navigator.of(context)
                                      .popUntil((route) => route.isFirst);
                                  lastState.onTimer();
                                });
                      },
                    ))
              ],
            )));
  }
}

class RegistrationMenu extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _nicknameController = TextEditingController();
  final _nameController = TextEditingController();
  final _charnameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(title: new Text("Введите данные регистрации")),
        body: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(hintText: "Введите имя"),
                ),
                TextFormField(
                  decoration:
                      const InputDecoration(hintText: "Введите никнейм"),
                  controller: _nicknameController,
                ),
                TextFormField(
                  decoration:
                      const InputDecoration(hintText: "Введите имя пресонажа"),
                  controller: _charnameController,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: StreamBuilder<StateStore?>(
                      initialData: lastState,
                      stream: stateStreamController.stream,
                      builder:
                          (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
                            return getErrorWidgetSnap(state) ??
                            ElevatedButton(
                              child: Text("Зарегистрироваться"),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  net
                                      .register(
                                          _nameController.text,
                                          _nicknameController.text,
                                          _charnameController.text,
                                          "")
                                      .then((ans) {
                                    Navigator.of(context)
                                        .pushNamed("/login", arguments: ans);
                                  });
                                }
                              },
                            );
                      },
                    ))
              ],
            )));
  }
}

class CharacterMenu extends StatelessWidget {
  static const TextStyle style = TextStyle(fontSize: 16.0);

  Widget getElem(String text) {
    return ListTile(title: Text(text));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [loadIndicatorBuilder()],
          title: Text("Карточка персонажа"),
        ),
        body: StreamBuilder<StateStore?>(
          initialData: lastState,
          stream: stateStreamController.stream,
          builder: (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
            var errorWidget = getErrorWidgetSnap(state);
            if (errorWidget != null) return errorWidget;
            if (state.data == null) {
              return Text("Нет данных");
            }
            List<Widget> elems = [];

            if (state.data!.player != null) {
              elems.add(QrImage(
                data: "p${state.data!.player!.id.toString().padLeft(6, '0')}",
                version: QrVersions.auto,
                size: 200,
                backgroundColor: Color.fromARGB(255, 255, 255, 255),
              ));
              elems.add(getElem("Имя игрока:${lastState.player!.nickname}"));
            }
            if (state.data!.char != null) {
              elems.add(getElem("Имя персонажа: ${lastState.char!.name}"));

              for (CharPropertyBase prop in configuration.charProperty) {
                Widget? w = prop.getWidget(state.data!, ctx);
                //getWidget может вернуть null если ничего рисовать не надо
                if (w != null) elems.add(w);
              }
            }

            return ListView(children: elems);
          },
        ));
  }

//

}

class Selector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CharPropertySelector prop =
        ModalRoute.of(context)?.settings.arguments as CharPropertySelector? ??
            CharPropertySelector.undefined();
    return Scaffold(
        appBar: AppBar(
          actions: [loadIndicatorBuilder()],
          title: Text("Выбор ${prop.label}"),
        ),
        body: StreamBuilder<StateStore?>(
            initialData: lastState,
            stream: stateStreamController.stream,
            builder: (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
              var errorWidget = getErrorWidgetSnap(state);
              if (errorWidget != null) return errorWidget;
              if (state.data == null) {
                return Text("Нет данных");
              }
              List<Widget> elems = [];
              List<String> path = prop.transitionPrefix.split("-");
              print("Loading races from $path}");
              for (TransitionDesc tr
                  in state.data?.transTree.getElement(path)?.getList() ?? []) {
                late Widget trailer;
                if (!tr.loaded) {
                  tr.load();
                  trailer = CircularProgressIndicator();
                } else {
                  if (tr.passed)
                    trailer = ElevatedButton(
                        onPressed: () {
                          tr.apply();
                          Navigator.of(context).pop();
                        },
                        child: const Text("Взять"));
                  else
                    trailer = Text(tr.getFailedCmp());
                }
                elems.add(ListTile(
                  title: Text(tr.title),
                  subtitle: Text(tr.description),
                  trailing: trailer,
                ));
              }

              return ListView(
                children: elems,
              );
            }));
  }
}

class QRScannerResult {
  String? code;
  int? number;
  Stream<List<QRAction>>? actions;
  String? name;
}

class QRScanner extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => QRScannerState();
}

class QRScannerState extends State<QRScanner> with TickerProviderStateMixin {
  QRCaptureController _captureController = QRCaptureController();
  late Animation<Alignment> _animation;
  late AnimationController _animationController;
  bool isCaptured = false;

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation =
        AlignmentTween(begin: Alignment.topCenter, end: Alignment.bottomCenter)
            .animate(_animationController);
    _animation.addListener(() {
      setState(() {});
    });
    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed)
        _animationController.reverse();
      else if (status == AnimationStatus.dismissed)
        _animationController.forward();
    });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _captureController.onCapture((str) => captured(context, str));
    var imgbg = Image.asset("images/sao@3x.png");
    var imgline = Image.asset("images/tiao@3x.png");
    return Scaffold(
      appBar: AppBar(
        title: Text("Сканируем QR"),
      ),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          QRCaptureView(
            controller: _captureController,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 56),
            child: AspectRatio(
              aspectRatio: 254 / 258.0,
              child: imgbg,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 56),
            child: AspectRatio(
              aspectRatio: 254 / 258.0,
              child: Stack(
                alignment: _animation.value,
                children: <Widget>[imgline],
              ),
            ),
          )
        ],
      ),
    );
  }

  void captured(BuildContext ctx, String str) {
    if (isCaptured) return;
    QRScannerResult res = QRScannerResult();
    if (str.length != 7) {
      print("QRCode check error: string $str not 7 letters length");
      return;
    }
    if (!str.startsWith(RegExp('[a-z]'))) {
      print("QR code check  error: code shut be start from letter $str  ");
      return;
    }
    res.code = str[0];
    res.number = int.tryParse(str.substring(1));
    if (res.number == null) {
      print("QR code check error: can`t parse number $str");
      return;
    }
    isCaptured = true;
    Navigator.of(ctx).pop(res);
  }
}

class QRActionMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    QRScannerResult res =
        ModalRoute.of(context)!.settings.arguments as QRScannerResult;

    return Scaffold(
      appBar: AppBar(title: Text("Дейсвтия с ${res.name}")),
      body: StreamBuilder(
        stream: res.actions,
        builder: (BuildContext ctx, AsyncSnapshot<List<QRAction>> snapshot) {
          List<Widget> widgets = [];
          var error = getErrorWidget(lastState);
          if (error != null) return error;
          if (snapshot.data == null) {
            widgets.add(ListTile(
                leading: Icon(Icons.error),
                title: Text(
                    "Не могу найти действия для кода ${res.code} ${res.number}")));
          } else {
            for (QRAction act in snapshot.data!) {
              if (act.description != null) {
                widgets.add(ListTile(
                  title: Text(act.description!),
                ));
              } else {
                widgets.add(ElevatedButton(
                  child: Text(act.name),
                  onPressed: (act.func == null
                      ? null
                      : () => act.func!(res.number!, context)),
                ));
              }
            }
          }

          return ListView(children: widgets);
        },
      ),
    );
  }
}

class SettingsWidget extends StatelessWidget {
  void onExit(BuildContext ctx) {
    showDialog(
        context: ctx,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Вы действительно хотите выйти?"),
            actions: <Widget>[
              TextButton(
                child: Text("Да"),
                onPressed: () {
                  net.resetCookie();
                  lastState.reset();
                  loadManager.startLoading();
                },
              ),
              TextButton(
                child: Text("Нет"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  void onLogout(BuildContext ctx) {
    showDialog(
        context: ctx,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Вы действительно хотите  сменить пользователя?"),
            actions: <Widget>[
              TextButton(
                child: Text("Да"),
                onPressed: () {
                  net.resetCookie();
                  lastState.logout();
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
              ),
              TextButton(
                child: Text("Нет"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> elements = [];

    elements.add(ListTile(
      title: Text("Префикс игры ${configuration.prefsPrefix}"),
      subtitle: Text("Serial ${configuration.serial}"),
    ));

    for (var s in configuration.servers) {
      Widget caption;
      if (s.isActive)
        caption = Text("Активный сервер");
      else
        caption = Text("Сервер");
      elements.add(ListTile(
        title: caption,
        subtitle: Text("${s.path} rq ${s.ok}/${s.error} "),
      ));
    }
    elements.add(ListTile(
        title: Text(
            "${net.webSocketStatus.toString()} ${net.webSocket?.readyState}")));
    if (kDebugMode) {
      elements.add(ListTile(
        title: Text("===Тест ==="),
        onTap: () {
          var res = QRScannerResult();
          res.code = "r";
          res.number = 2;
          configuration.qrActions[res.code!]?.updateQResult(res);
          Navigator.of(context).pushNamed("/qractions", arguments: res);
        },
      ));
      elements.add(ListTile(
        title: Text("===Тест2 ==="),
        onTap: () {
          var res = QRScannerResult();
          res.code = "m";
          res.number = 1;
          configuration.qrActions[res.code!]?.updateQResult(res);
          Navigator.of(context).pushNamed("/qractions", arguments: res);
        },
      ));
    }
    elements.add(ListTile(
      leading: Icon(Icons.exit_to_app),
      title: Text("Разлогиниться"),
      onTap: () => onLogout(context),
    ));
    elements.add(ListTile(
      leading: Icon(Icons.exit_to_app),
      title: Text("Выйти из игры"),
      onTap: () => onExit(context),
    ));
    return Scaffold(
        appBar: AppBar(title: Text("Настройки")),
        body: ListView(
          children: elements,
        ));
  }
}

class Hacking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HackingState();
}

var _random = Random();

class HackingElement {
  late double x, y;
  late int img;
  bool open = false;
  bool locked = false;
  late HackingElement peer;

  HackingElement.generate(int i) {
    img = i;
    x = 30 + _random.nextInt(300).toDouble();
    y = 10 + _random.nextInt(500).toDouble();
  }
}

enum HackingResult { DONE, CANCELED, TIMEOUT }

//TODO: рефактор логики взлома
class HackingRequest {
  Function? onDone;
  DateTime? timeout;

  HackingRequest.def() {
    onDone = null;
  }

  HackingRequest.timeout(int seconds) {
    timeout = DateTime.now().add(Duration(seconds: seconds));
  }

  HackingRequest(Function f) {
    onDone = f;
  }

  void done() {
    if (onDone != null) onDone!();
  }
}

class HackingState extends State<Hacking> {
  List<HackingElement> elements = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 5; i++) {
      var a = HackingElement.generate(i);
      var b = HackingElement.generate(i);
      a.peer = b;
      b.peer = a;
      elements.add(a);
      elements.add(b);
    }
  }

  void end(BuildContext context, HackingResult result, HackingRequest request) {
    String caption = "";
    String button = "";
    if (result == HackingResult.DONE) {
      caption = "Взлом удался";
      button = "Ура!";
    } else if (result == HackingResult.TIMEOUT) {
      caption = "Взлом НЕ удался";
      button = "понятно";
    }
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text(caption),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop();
                    Navigator.of(ctx).pop(result);
                    if (result == HackingResult.DONE) request.done();
                  },
                  child: Text(button))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    HackingRequest request =
        ModalRoute.of(context)!.settings.arguments as HackingRequest? ??
            HackingRequest.def();
    var background = Image.asset(
      "images/background.png",
    );
    var unknown = Image.asset(
      "images/unknown.png",
      scale: 2,
    );
    var known = [
      Image.asset(
        "images/Antenna.png",
        scale: 2,
      ),
      Image.asset(
        "images/capasitor.png",
        scale: 2,
      ),
      Image.asset(
        "images/cerber.png",
        scale: 2,
      ),
      Image.asset(
        "images/ground.png",
        scale: 2,
      ),
      Image.asset(
        "images/resistor.png",
        scale: 2,
      ),
    ];

    bool isSuccess() {
      for (var e in elements) {
        if (!e.locked) return false;
      }
      return true;
    }

    List<Widget> widgets = [background];
    for (var e in elements) {
      widgets.add(Padding(
        padding: EdgeInsets.fromLTRB(e.x, 0, 0, e.y),
        child: GestureDetector(
            onTap: () {
              setState(() {
                if (e.peer.open) {
                  e.peer.locked = true;
                  e.locked = true;
                }
                elements.forEach((element) {
                  element.open = element.locked;
                });
                e.open = true;
                if (isSuccess()) {
                  end(context, HackingResult.DONE, request);
                }
              });
            },
            child: e.open ? known[e.img] : unknown),
      ));
    }
    String header = "Взлом";
    if (request.timeout != null) {
      Duration delta = request.timeout!.difference(DateTime.now());

      header += " (осталось ${delta.inSeconds} c)";
      Timer(
          Duration(seconds: 1),
          () => setState(() {
                if (delta.inSeconds <= 0) {
                  end(context, HackingResult.TIMEOUT, request);
                  request.timeout = null;
                }
              }));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(header),
      ),
      body: Stack(
        alignment: Alignment.bottomLeft,
        children: widgets,
      ),
    );
  }
}

class SkillContext {
  late List<String> path;

  SkillContext.empty() {
    path = [];
  }

  SkillContext(List<String> p) {
    path = p;
  }
}

//TODO: где то должна отображаться стоимость скила
class Skills extends StatelessWidget {
  List<Widget> generateSkillButtons(List<TransitionDesc> trans) {
    List<Widget> widgets = [];
    for (TransitionDesc? t in trans) {
      Widget? traling;
      if (t!.loaded) {
        if (t.passed)
          traling =
              ElevatedButton(onPressed: () => t.apply(), child: Text("Взять"));
        else {
          traling = null; //Text(t.getFailedCmp(sep: "\n"));
        }
      } else {
        t.load();
        traling = CircularProgressIndicator();
      }
      String title = t.title;
      if (!t.passed) {
        title += "(невозможно взять: " + t.getFailedCmp() + ")";
      }
      widgets.add(ListTile(
        title: Text(title),
        subtitle: Text(t.description),
        trailing: traling,
      ));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    SkillContext ctx =
        ModalRoute.of(context)!.settings.arguments as SkillContext? ??
            SkillContext.empty();
    return StreamBuilder<StateStore?>(
        initialData: lastState,
        stream: stateStreamController.stream,
        builder: (BuildContext context, AsyncSnapshot<StateStore?> shot) {
          var errorWidget = getErrorWidgetSnap(shot);
          if (errorWidget != null) return errorWidget;

          List<Widget> widgets = [];
          String classFeature = configuration.skills.classFeature;
          String crossClassFeature = configuration.skills.crossClassFeature;

          if (classFeature == "" || ctx.path.isNotEmpty) {
            var prefix = [configuration.skills.transPrefix];

            if (ctx.path.isNotEmpty) prefix = ctx.path;

            var trans = shot.data!.transTree.getElement(prefix)?.getList();
            widgets = generateSkillButtons(trans ?? []);
          } else {
            widgets += getSkillWidgets(shot.data!, context, classFeature, "",
                configuration.skills.transPrefix);

            if (crossClassFeature != "") {
              widgets += getSkillWidgets(
                  shot.data!,
                  context,
                  crossClassFeature,
                  " (дополнительный)",
                  configuration.skills.crossSkillGetPrefix);
            }
          }

          if (widgets.isEmpty)
            widgets.add(ListTile(title: Text("Нет доступных навыков")));

          var fd = shot.data!.char!
              .featuresByTypeName[configuration.skills.expFeature]?.first;

          String exp = "";
          if (fd != null) exp = "${fd.valueStr} ед. опыта свободно";

          return Scaffold(
              appBar: AppBar(
                title: Text("Навыки $exp"),
              ),
              body: ListView(children: widgets));
        });
  }

  List<Widget> getSkillWidgets(StateStore state, BuildContext context,
      String classFeature, String suffix, String transPrefix) {
    List<Widget> widgets = [];
    List<String> classes = state.char!.featuresByTypeName[classFeature]
            ?.map((e) => e.valueStr)
            .toList() ??
        [];

    TreeElement<String, TransitionDesc>? skills =
        state.transTree.getElement([transPrefix]);

    if (skills == null) return [];

    for (var kv in skills.trunk!.entries) {
      if (!classes.contains(kv.key)) continue;

      widgets.add(ListTile(
        title: Text(kv.key + suffix),
      ));

      for (var skills in kv.value.trunk!.entries) {
        widgets.add(ElevatedButton(
            child: Text(skills.key),
            onPressed: () {
              List<String> path = [transPrefix, kv.key, skills.key];
              Navigator.of(context)
                  .pushNamed("/skills", arguments: SkillContext(path));
            }));
      }
    }
    return widgets;
  }
}

class SelectCrossClass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Выберете доп класс")),
      body: StreamBuilder(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];

          var errorWidget = getErrorWidgetSnap(shot);
          if (errorWidget != null) return errorWidget;

          if (configuration.skills.crossTransPrefix == "")
            return Text("В данной конфигурации нет дополнительных классов");
          List<String> prefixPath =
              configuration.skills.crossTransPrefix.split("-");
          var tree = shot.data!.transTree.getElement(prefixPath);
          if (tree == null) return Text("Доступных классов не обнаружено");
          for (var classTrans in tree.getList()) {
            late Widget button;
            if (!classTrans.loaded) {
              button = CircularProgressIndicator();
              classTrans.load();
            } else {
              if (classTrans.passed)
                button = ElevatedButton(
                    onPressed: () => classTrans.apply(), child: Text("Взять"));
              else
                button = Text("Невозможно " + classTrans.getFailedCmp());
            }

            widgets.add(ListTile(
                title: Text(classTrans.title),
                subtitle: Text(classTrans.description),
                trailing: button));
          }

          return ListView(children: widgets);
        },
      ),
    );
  }
}

class BLEPeripheralsMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Оборудование"),
      ),
      body: StreamBuilder(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];
          var errorWidget = getErrorWidgetSnap(shot);
          if (errorWidget != null) return errorWidget;
          if (configuration.blePeripheralsUUID == "")
            return Text("Внешнеее устройство не настроено");
          StateStore state = shot.data!;
          if (state.bleConnection == null) {
            if (state.bleDeviceMAC == null ||
                state.bleDeviceMAC!.valueStr.isEmpty) {
              widgets.add(ListTile(
                title: const Text("Соедение не установлено"),
                trailing: ElevatedButton(
                  child: Text("Соеденить"),
                  onPressed: () => Navigator.of(context).pushNamed("/blescan"),
                ),
              ));
            } else {
              widgets.add(ListTile(
                title: Text("Нет соединения с ${state.bleDeviceMAC!.valueStr}"),
                trailing: ElevatedButton(
                  child: Text("Поиск другого"),
                  onPressed: () => Navigator.of(context).pushNamed("/blescan"),
                ),
              ));
            }
            return ListView(children: widgets);
          }

          widgets.add(ListTile(
              title: Text("Соединение с ${state.bleConnection!.dev.id}")));
          for (var ch in state.bleConnection!.links) {
            String text = ch.conf.label;

            text += " " + ch.valueStr;

            if (ch.feat != null) {
              text += " " + ch.feat!.valueStr;
            }

            widgets.add(ListTile(title: Text(text)));
          }
          return ListView(children: widgets);
        },
      ),
    );
  }
}
